//JXG.Options.renderer = 'canvas';
var JSXMaster = {
	fakePoint: undefined, //this point is the position of mouse on the screen (always)
	fakeText: undefined,
	board: undefined,
	scale: 100,
	mapWidth: undefined,
	mapHeight: undefined,
	
	/** style of points on the map **/
	pointStyle: {fillColor:'#00ff00', strokeWidth:0, size:3, name:'', showInfobox: false},
	
	/** style of text on the map **/
	textStyle:{fontSize: 10, display: 'html', fixed: true},
	
	/** the current drawing mode on the map, possible values: 1,2,3,4,5 **/
	SHAPE_MODE : undefined,
	/** all possible modes on the map **/
	SHAPING_MODES: {'line' : 1, 'circle' : 2, 'polygon': 3, 'rectangle': 4, 'dragging': 5, 'text': 6},
	
	/** structure: {cls: ..., type:.....}
		@cls: Polygon, Line, Circle, Rectangle, Text
		@type: possible values: 'polygon', 'line', 'circle', 'rectangle', 'text' 
	**/
	activeDrawingMode: undefined,
	
	/** structure: {cls:....., activeId:....., type:......}
		@cls: Polygon, Line, Circle, Rectangle, Text
		@activeShapeId: string.
		@type: possible values: 'polygon', 'line', 'circle', 'rectangle', 'text' **/
	activeShape: undefined, 
	setActiveShape: function(cls, id, type){
		console.log('setActiveShape: ' + id + " " + type);
		JSXMaster.activeShape = {cls : cls, activeShapeId: id, type: type};
	},
	getActiveShape: function(){
		var self = JSXMaster;
		return self.activeShape;
	},
	showToolbar: function(){
		if(JSXMaster.activeDrawingMode !== undefined){
			JSXMaster.activeDrawingMode.cls.showHideToolbar('show', JSXMaster.activeDrawingMode.type);
		}else{
			gui.Drag();
		}
	},
	deleteActiveShape: function(){
		//TODO
		var active = JSXMaster.activeShape;
		if(active !== undefined){
			active.cls.removeShape(active.activeShapeId);
			JSXMaster.activeShape = undefined;
			//gui.ActivateItem('');
		}
	},
	deactivateAll: function(){
		var record = JSXMaster.activeShape;
		//console.log('JSXMaster.deactivateAll: ' + JSON.stringify(record));
		
		if(record !== undefined){
			console.log('JSXMaster.deactivateAll: ' + record.activeShapeId + " " + record.type);
			var cls = record['cls'],
				activeId = record['activeShapeId'];
			//alert("ton tai thuoc tinh ko: " + cls.hasOwnProperty('deactivateShape'));
			if(cls !== undefined && cls.hasOwnProperty('deactivateShape')){
				//alert('dkm');
				cls.deactivateShape(activeId);
				//gui.ActivateItem('');
				JSXMaster.activeShape = undefined;
			}
		}
	},
	deactivateExcept: function(currActive){
		var r = JSXMaster.activeShape;
		if(r !== undefined && r.activeShapeId !== currActive){
			JSXMaster.deactivateAll();
			
		}
	},
	activateAll: function(){
		var record = JSXMaster.activeShape;
		if(record !== undefined){
			var cls = record['cls'],
				activeId = record['activeShapeId'];
			if(cls !== undefined && cls.hasOwnProperty('activeShape')){
				cls.activateShape(activeId);
				//gui.ActivateItem(cls.type);
			}
		}
	},
	setDrawingMode: function(cls, type){
		(cls === undefined) ? 
			JSXMaster.activeDrawingMode = undefined : 
			JSXMaster.activeDrawingMode = {cls: cls, type: type};
		//JSXMaster.SHAPE_MODE = JSXMaster.SHAPING_MODES[type];
	},
	init : function(){
		$("#gui").css("height","850px");
		var wiwidth=$("#main-content .portlet-dropzone").width();
		var wiheight=$("#main-content .portlet-dropzone").height();
		$("#gui").css("height",wiheight+"px");
		JSXMaster.mapWidth = wiwidth/JSXMaster.scale;
		JSXMaster.mapHeight = wiheight/JSXMaster.scale;
		var w = JSXMaster.mapWidth, h = JSXMaster.mapHeight;
		//var dim = (w > h) ? h : w;
		//dim = dim/100;
		console.log("init:" +w.toString()+" "+h.toString()+ " "+JSXMaster.scale.toString()+" "+wiwidth.toString()+" "+wiheight.toString());
		JSXMaster.board = JXG.JSXGraph.initBoard('jxgbox', {boundingbox: [-w/2,h/2,w/2,-h/2], 
			axis: false, 
			grid: true, 
			showCopyright: false, 
			showNavigation: true,
			zoom: {
				wheel: true
			}
		});
		JSXMaster.fakePoint = JSXMaster.board.create('point', [0, 0], JSXMaster.pointStyle);	
		JSXMaster.fakeText = JSXMaster.board.create('text', [0, 0, ''], JSXMaster.textStyle);
		JSXMaster.hideFakePnt();
		
/*		$.post("http://localhost:8080/web/guest/image-list?p_p_id=imagelist_WAR_Biziwaveportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_cacheability=cacheLevelPage&p_p_col_id=column-1&p_p_col_count=1", "", function(returnedData) {*/
		$.post("", "", function(returnedData) {
			var lists= jQuery.parseJSON(returnedData);
			var pId= getParameterByName("pId");
			if (pId!=null){
				for (i=0;i<lists.length;i++){
					if (lists[i]["img_id"]==pId){
						var photo= lists[i]["img_photo"];
						var img = new Image();
						/*img.src = 'data:image/png;base64,'+photo;*/
						img.src = photo;
						ImageOBJ.setBackground(img);
					}
				}
			}
			//ImageOBJ.setBackground(img);
		})
		
		
	},
	getMouseCoords : function(e, i) {
		var cPos = JSXMaster.board.getCoordsTopLeftCorner(e, i),
			absPos = JXG.getPosition(e, i),
			dx = absPos[0]-cPos[0],
			dy = absPos[1]-cPos[1];
		//console.log("update mouse coord");
		var res = new JXG.Coords(JXG.COORDS_BY_SCREEN, [dx, dy], JSXMaster.board);
		JSXMaster.currentMouseCoords = res.usrCoords.slice(1);
		return res;
	},
	currentMouseCoords: undefined,
	hideFakePnt: function(){
		if(JSXMaster.fakePoint !== undefined)
			JSXMaster.fakePoint.hideElement();
	},
	showFakePnt: function(){
		var shouldShow = false;
		var activeDrawingMode = JSXMaster.activeDrawingMode;
		if(activeDrawingMode !== undefined){
			var type = activeDrawingMode.type;
			if( type === 'line' || 
				type === 'circle' || 
				type === 'polygon' || 
			    type === 'rectangle')
			{
				shouldShow = true;
			}
			
		}
		
		
		if(shouldShow){
			JSXMaster.fakePoint.showElement();
		}
	},
	mousewheel: function(evt){
		console.log("wheeling");
	},
	
	disableBoardEvent: function(){
		JSXMaster.board.off('down', Circle.down);
		JSXMaster.board.off('down', Line.down);
		JSXMaster.board.off('down', Rectangle.down);
		JSXMaster.board.off('down', Polygon.down);
		JSXMaster.board.off('down', TextOBJ.down);
	},
	enableBoardEvent: function(){
		//There is a 'down' event associated with the board.
		if(JSXMaster.board.eventHandlers.down !== undefined){
			return;
		}
		if(JSXMaster.activeDrawingMode === undefined){
			return;
		}
		var activeDraw = JSXMaster.activeDrawingMode;
		//if(activeDraw.type === 'line' || activeDraw.type === 'circle' || 
		//	activeDraw.type === 'polygon')
		if(activeDraw.cls.hasOwnProperty('down'))
		{
			JSXMaster.board.on('down', activeDraw.cls.down);
		}
		if(activeDraw.cls.hasOwnProperty('restoreStyle')){
			activeDraw.cls.restoreStyle();
		}
		
	}
	
};
JSXMaster.init();

var Polygon = {
	currLine: undefined,
	prefix: "poly",
	counter: 0,
	
	/**
	@field: polygons
	@type: dictionary of polygons drawn on the screen.
	@structure: key: polygonId, value: an array of vertices belonging to @polygonId 
	@aim: many things. 
	@example: {poly1: [v1, v2,...,vn], poly2: [], ....}
	*/
	polygons: {}, 
	
	/**
	@field: lines
	@structure: key: polygonId, value: an array of edges belonging to @polygonId
	@type: dictionary of edges of a polygon.
	@aim: the set of lines belonging to the current polygon. This set is used only for binding events.
	*/
	lines: {}, 
	
	/**
	@field: mappingPoints 
	@type: dictionary
	@structure: key: pointID, value: polygonID. 
	@aim: This dictionary is used for lookup a polygon that a vertex belongs to.
	*/
	mappingPoints: {}, 
	
	init: function(){
		Polygon.mappingPoints = {};
		Polygon.lines = {};
		Polygon.polygons = {};
		Polygon.counter = 0;
		Polygon.currLine = undefined;
	},
	removeShape : function(polyId){
		//TODO
		//alert(polyId);
		var pts = Polygon.polygons[polyId];
		var lines = Polygon.lines[polyId];
		for(var i=0; i<lines.length; i++){
			gui.deleteObj(lines[i]);
		}
		for(var i=0; i<pts.length; i++){
			delete Polygon.mappingPoints[pts[i].id];
			gui.deleteObj(pts[i]);
		}
		delete Polygon.lines[polyId];
		delete Polygon.polygons[polyId];
	},
	down : function(e) {
		//handler when we press the mouse on the screen.
		var canCreate = true, 
			coords, 
			polygonID, 
			points;
 
		
		coords = JSXMaster.getMouseCoords(e, 0);
		polygonID = Polygon.prefix + "_" + Polygon.counter;
		//if(JSXMaster.activeShape.activeShapeId !== polygonID)
		//	JSXMaster.deactivateAll();
		JSXMaster.deactivateExcept(polygonID);
		JSXMaster.showToolbar();
		//alert(polygonID);
		if(Polygon.ableToCreate(polygonID, e)){
	
			console.log("Can create");
			var first = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], JSXMaster.pointStyle);
			
			if(Polygon.currLine !== undefined){
				//complete the previous line. 
				Polygon.currLine.point2 = first;
			}
			Polygon.currLine = JSXMaster.board.create('line', [first, JSXMaster.fakePoint], 
										{straightFirst:false, straightLast:false, strokeWidth:2});
									
			//set of lines of current polygon.
			Polygon.lines[polygonID] = Polygon.lines[polygonID] || [] ;
			Polygon.lines[polygonID].push(Polygon.currLine);
			//Polygon.lines[polygonID] = list;
			
			//add the new vertex to the polygon @polygonID
			Polygon.polygons[polygonID] = Polygon.polygons[polygonID] || [] ;
			Polygon.polygons[polygonID].push(first);
			
			//this vertex belongs to @polygonID
			Polygon.mappingPoints[first.id] = polygonID;
			
		}
	},
	initHoverBinding: function(polyId){
		var lines = Polygon.lines[polyId] || [];
		for(var i=0; i<lines.length; i++){
			lines[i].on('over', Polygon.overLine);
			lines[i].on('out', Polygon.outLine);
		}
		
	},
	overLine : function(e){
		//console.log("on element");
		var self = this;
		//console.log(JSON.stringify(self.attrs));
		//self.withLabel = true;
		//JSXMaster.fakePoint.hideElement();
		JSXMaster.disableBoardEvent();
		//JSXMaster.fakePoint.hideElement();
		JSXMaster.hideFakePnt();
		//JSXMaster.fakeText.hideElement();
		JSXMaster.board.containerObj.style.cursor = 'move';
		
		var p1 = self.point1, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X())/2, (p1.Y() + p2.Y())/2];
		sum = v[0] * v[0] + v[1] * v[1];
		var dist = Math.sqrt(sum);
		dist = Math.round(dist*100)/100;
		//alert(dist);
		JSXMaster.fakeText.setText( "<strong>"+dist+"</strong>");
		//console.log(JSON.stringify(mid));
		JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
		JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
		JSXMaster.fakeText.showElement();
		JSXMaster.board.fullUpdate();
	},
	outLine: function(e){
		//console.log("out element");
		var self = this;
		//self.withLabel = false;
		JSXMaster.enableBoardEvent();
		//JSXMaster.fakePoint.showElement();
		JSXMaster.showFakePnt();
		JSXMaster.fakeText.hideElement();
		//JSXMaster.board.containerObj.style.cursor = 'default';
	},
	restoreStyle: function(){
		console.log('restore');
		JSXMaster.board.containerObj.style.cursor = 'default';
	},
	/**
		Check if we can create new point and new line.
		@polygonID
		@event e: down event object
	*/
	ableToCreate : function(polygonID, e){
		var coords, points, canCreate = true;
		
		coords = JSXMaster.getMouseCoords(e, 0);
		points = Polygon.polygons[polygonID] || [];
		for(var i=0; i<points.length; i++){
			
			var point = points[i];
			
			//The condition that inform us that a cycle (a polygon) is created.
			if(point.id !== JSXMaster.fakePoint.id && point.hasPoint(coords.scrCoords[1], coords.scrCoords[2]))
			{
				Polygon.completePolygon(point);
				//Polygon.hideVertices(polygonID);
				Polygon.activateShape(polygonID);
				Polygon.initHoverBinding(polygonID, true);
				Polygon.initDraggingListener(polygonID, true);
				canCreate = false;
				break;
			}
		}
		return canCreate;
	},
	/**a cycle of points was created. 
	we need to complete the current polygon and reset data for the new one*/
	completePolygon: function(lastPoint){
		Polygon.currLine.point2 = lastPoint;
		Polygon.currLine = undefined;
		Polygon.counter++; //increase the id of polygon
	},
	showVertices: function(polyId){
		//show all vertices when being active
		var pts = Polygon.polygons[polyId];
		for(var i=0; i<pts.length; i++){
			var p = pts[i];
			p.showElement();
		}
	},
	hideVertices: function(polyId){
		//hide all vertices, when being inactive
		//show all vertices when being active
		var pts = Polygon.polygons[polyId];
		for(var i=0; i<pts.length; i++){
			var p = pts[i];
			p.hideElement();
		}
	},
	mouseDownEdge: function(e){
		console.log('click edge of polygon');
		gui.ActivateItem('polygon');
		
		JSXMaster.getMouseCoords(e, 0);
		var self = this, 
			polygonID = Polygon.mappingPoints[self.point1.id];
		//Polygon.lines[polygonID][0].getAttributes();
		//if(JSXMaster.activeShape.activeShapeId !== polygonID)
		//	JSXMaster.deactivateAll();
		Polygon.setToolBar(polygonID);
		JSXMaster.deactivateExcept(polygonID);
		Polygon.activateShape(polygonID);
	},
	setToolBar: function(polyId){
		var attrs = Polygon.lines[polyId][0].getAttributes();
		gui.DotActive("");
		gui.SolidActive("");
		gui.DashActive(""); 
		if(attrs.dash === 2){
			gui.DashActive("toolbar_item_click"); 
		}else if(attrs.dash === 1){
			gui.DotActive("toolbar_item_click");
		}else if(attrs.dash === 0){
			gui.SolidActive("toolbar_item_click");
		}
		//TODO
		//gui.PolygonStrokeWidth PolygonColorValue
		//alert(attrs.strokewidth);
		gui.PolygonStrokeWidth(attrs.strokewidth);
		//alert(attrs.strokecolor);
		gui.PolygonColorValue(attrs.strokecolor);
		document.getElementById(gui.PolygonColorInputId()).color.fromString(gui.PolygonColorValue());
	},
	activateShape: function(polyId){
		JSXMaster.setActiveShape(Polygon, polyId, 'polygon');
		Polygon.showVertices(polyId);
	},
	deactivateShape: function(polyId){
		//console.log('start');
		Polygon.hideVertices(polyId);
	},
	showHideToolbar: function(action, type){
		if(action === 'hide'){
			gui.ActivateItem('');
		}else{
			(type === undefined) ? type = 'polygon' : type = type;
			gui.ActivateItem(type)
		}
	},
	/*
	mouseUpEdge: function(e){
		//gui.ActivateItem('');
		//JSXMaster.getMouseCoords(e, 0);
		var self = this, 
			polygonID = Polygon.mappingPoints[self.point1.id];
		Polygon.hideVertices(polygonID);
	},
	/**
		{boolean} @realTime: determine whether binding real-time or loading from file
		{string} @polygonID
		Binding dragging event for each edge
	*/
	initDraggingListener: function(polygonID, realTime){
		var lines = Polygon.lines[polygonID] || [];
				
		for(var j = 0; j<lines.length; j++){
			var line = lines[j];
			line.on('down', Polygon.mouseDownEdge);
			//line.on('up', Polygon.mouseUpEdge);
			if(realTime === undefined || realTime === false)
				//not real-time
				line.on('drag', Polygon.dragging2);
			else
				line.on('drag', Polygon.dragging);
		}
	},
	dragging2: function(e){
		//function is used for binding when loading from file
		var self = this, 
			polygonID = Polygon.mappingPoints[self.point1.id],
			points = Polygon.polygons[polygonID] || [];
		//alert("polygonID: " + polygonID);
		//alert(JSON.stringify(points));
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;
		JSXMaster.fakeText.hideElement();
		//console.log("begin point: " + JSON.stringify(begin));
		//console.log("end point: " + JSON.stringify(end));
		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var first = this.point1, 
			second = this.point2;
		//first.prepareUpdate().update().updateRenderer();
		//second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
		//second.prepareUpdate().update().updateRenderer();
		
		//first.setPosition(JXG.COORDS_BY_USER, [first.X() + vector.x, first.Y() + vector.y]);
		for(var k=0; k<points.length; k++){
			var nextPts = points[k];
			if(nextPts.id != first.id && nextPts.id != second.id){
				nextPts.setPosition(JXG.COORDS_BY_USER, [nextPts.X() + vector.x, nextPts.Y() + vector.y]);
				nextPts.prepareUpdate().update().updateRenderer();
			} 
		}
	},
	dragging: function(e){
		//this: the line that we drag
		var self = this, 
			polygonID = Polygon.mappingPoints[self.point1.id],
			points = Polygon.polygons[polygonID] || [];
		//alert("polygonID: " + polygonID);
		//alert(JSON.stringify(points));
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;
		JSXMaster.fakeText.hideElement();
		//console.log("begin point: " + JSON.stringify(begin));
		//console.log("end point: " + JSON.stringify(end));
		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var first = this.point1, 
			second = this.point2;
		//first.prepareUpdate().update().updateRenderer();
		second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
		second.prepareUpdate().update().updateRenderer();
		
		//first.setPosition(JXG.COORDS_BY_USER, [first.X() + vector.x, first.Y() + vector.y]);
		for(var k=0; k<points.length; k++){
			var nextPts = points[k];
			if(nextPts.id != first.id && nextPts.id != second.id){
				nextPts.setPosition(JXG.COORDS_BY_USER, [nextPts.X() + vector.x, nextPts.Y() + vector.y]);
				nextPts.prepareUpdate().update().updateRenderer();
			} 
		}
	},
	binding: function(){
		JSXMaster.board.on('down', Polygon.down);
		JSXMaster.board.on('mousemove', Polygon.mousemove);
		JSXMaster.board.on('mouseup', Polygon.mouseup);
	},
	unbinding: function(){
		JSXMaster.board.off('down', Polygon.down);
		JSXMaster.board.off('mousemove', Polygon.mousemove);
		JSXMaster.board.off('mouseup', Polygon.mouseup);
	},
	mousemove: function(e){
		var coords = JSXMaster.getMouseCoords(e, 0);
		JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
		JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
		if(Polygon.currLine !== undefined){
			Polygon.currLine.prepareUpdate().update().updateRenderer();
		}
		//http://jsxgraph.uni-bayreuth.de/docs/symbols/JXG.GeometryElement.html
	},
	mouseup: function(e){
	},
	/**@shapeType: solid, dash, dot of the boundary of each shape**/
	setShapeType: function(shapeType){
		//TODO
		var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Polygon.lines[activeId];
		for(var i=0; i<shape.length; i++){
			var line = shape[i];
			line.setAttribute({dash: shapeType});
		}
		
	},
	/**@width: width of lines of rect**/
	setStrokeWidth: function(width){
		var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Polygon.lines[activeId];
		for(var i=0; i<shape.length; i++){
			var line = shape[i];
			line.setAttribute({strokeWidth: width});
		}
	},
	/**@color: color of shape**/
	setShapeColor: function(color){
		var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Polygon.lines[activeId];
		for(var i=0; i<shape.length; i++){
			var line = shape[i];
			line.setAttribute({strokeColor: color});
		}
	}
};
var Circle = {
	circle: undefined,
	circles: {},
	//isBinding: false,
	circleStyle: {strokeColor:'#0000ff',strokeWidth:2},
	removeShape : function(cId){
		var c = Circle.circles[cId];
		var center = c.center,
			p2 = c.point2;
		gui.deleteObj(c);
		gui.deleteObj(center);
		gui.deleteObj(p2);
		delete Circle.circles[cId];
	},
	down: function(e){
		console.log("what");
		var coords = JSXMaster.getMouseCoords(e, 0);
					
		if(Circle.circle !== undefined && Circle.circle.center.hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
			return;
		}
		var center = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], JSXMaster.pointStyle);
		JSXMaster.deactivateExcept(center.id);
		JSXMaster.showToolbar();
		if(Circle.circle !== undefined){
			Circle.circle.point2 = center;
			Circle.bindEventForCircle(Circle.circle);
			
			Circle.circle = undefined;
			return;
		}
		Circle.circle = JSXMaster.board.createElement('circle',[center, JSXMaster.fakePoint], Circle.circleStyle);
	},
	bindEventForCircle: function(c){
		Circle.circles[c.center.id] = c;
		//Circle.hideVertices(c.center.id);
		Circle.activateShape(c.center.id);
		c.on('down', Circle.clickCircle);	
		c.on('drag', Circle.draggingCirle);
		c.on('over', Circle.hoverOn);
		c.on('out', Circle.hoverOff);
		c.center.on('down', Circle.clickCenter);
		c.center.on('drag', Circle.draggingCenter);
		c.center.on('over', Circle.hoverOn);
		c.center.on('out', Circle.hoverOff);
	},
	bindEventForCircle2: function(c){
		//This function is preserved for circle drawn from file.
		Circle.circles[c.center.id] = c;
		Circle.hideVertices(c.center.id);
		c.on('down', Circle.clickCircle);	
		//c.on('drag', Circle.draggingCirle);
		c.on('over', Circle.hoverOn);
		c.on('out', Circle.hoverOff);
		c.center.on('down', Circle.clickCenter);
		c.center.on('drag', Circle.draggingCenter);
		c.center.on('over', Circle.hoverOn);
		c.center.on('out', Circle.hoverOff);
	},
	hoverOn: function(e){
		//JSXMaster.board.off('down', Circle.down);
		JSXMaster.disableBoardEvent();
		//JSXMaster.fakePoint.hideElement();
		JSXMaster.hideFakePnt();
		JSXMaster.board.containerObj.style.cursor = 'move';
		var self = this;
		var p1 = self.center, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X())/2, (p1.Y() + p2.Y())/2];
		sum = v[0] * v[0] + v[1] * v[1];
		var dist = Math.sqrt(sum);
		dist = Math.round(dist*100)/100;
		//alert(dist);
		JSXMaster.fakeText.setText( "<strong>"+dist+"</strong>");
		//console.log(JSON.stringify(mid));
		JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, [self.point2.X(), self.point2.Y()]);
		JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
		JSXMaster.fakeText.showElement();
		JSXMaster.board.fullUpdate();
		
	},
	hoverOff: function(e){
		JSXMaster.enableBoardEvent();
		//JSXMaster.fakePoint.showElement();
		JSXMaster.showFakePnt();
		//JSXMaster.board.containerObj.style.cursor = 'default';
		JSXMaster.fakeText.hideElement();
	},
	restoreStyle: function(){
		console.log('restore');
		JSXMaster.board.containerObj.style.cursor = 'default';
	},
	draggingCirle: function(e){
		JSXMaster.fakeText.hideElement();
		var self = this;
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;

		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var first = this.center, 
			second = this.point2;
			
		second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
		second.prepareUpdate().update().updateRenderer();
	},
	clickCircle: function(e){
		//alert('fuck');
		//Circle.unbinding();
		JSXMaster.getMouseCoords(e, 0);
		gui.ActivateItem('circle');
		Circle.setToolBar(this.center.id);
		JSXMaster.deactivateExcept(this.center.id);
		Circle.activateShape(this.center.id);
	},
	setToolBar: function(polyId){
		var attrs = Circle.circles[polyId].getAttributes();
		gui.DotActive("");
		gui.SolidActive("");
		gui.DashActive(""); 
		if(attrs.dash === 2){
			gui.DashActive("toolbar_item_click"); 
		}else if(attrs.dash === 1){
			gui.DotActive("toolbar_item_click");
		}else if(attrs.dash === 0){
			gui.SolidActive("toolbar_item_click");
		}
		gui.CircleStrokeWidth(attrs.strokewidth);
		gui.CircleColorValue(attrs.strokecolor);
		document.getElementById(gui.CircleColorInputId()).color.fromString(gui.CircleColorValue());
	},
	clickCenter: function(e){
		JSXMaster.getMouseCoords(e, 0);
		gui.ActivateItem('circle');
		JSXMaster.deactivateExcept(this.id);
		Circle.activateShape(this.id);
	},
	showVertices: function(cId){
		var c = Circle.circles[cId];
		c.center.showElement();
		c.point2.showElement();
	},
	hideVertices: function(cId){
		var c = Circle.circles[cId];
		c.center.hideElement();
		c.point2.hideElement();
	},
	activateShape: function(circleId){
		//alert('fuck');
		JSXMaster.setActiveShape(Circle, circleId, 'circle');
		Circle.showVertices(circleId);
	},
	deactivateShape: function(circleId){
		Circle.hideVertices(circleId);
	},
	draggingCenter: function(e){
		var self = this;
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;
		
		//console.log("begin point: " + JSON.stringify(begin));
		//console.log("end point: " + JSON.stringify(end));
		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var main_circle = Circle.circles[self.id],
			second = main_circle.point2;
		//var second = this;
		//first.prepareUpdate().update().updateRenderer();
		second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
		second.prepareUpdate().update().updateRenderer();
	},
	move: function(e){
		var coords = JSXMaster.getMouseCoords(e, 0);
		JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
		//JSXMaster.fakePoint.getTextAnchor.update().updateRenderer();
		JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
		if(Circle.circle !== undefined){
			Circle.circle.prepareUpdate().update().updateRenderer();
		}
		
		//getTextAnchor
		
	},
	showHideToolbar: function(action, type){
		if(action === 'hide'){
			gui.ActivateItem('');
		}else{
			(type === undefined) ? type = 'circle' : type = type;
			gui.ActivateItem(type)
		}
	},
	up: function(e){
		//Circle.unbinding();
	},
	binding: function(){
		//Circle.isBinding = true;
		JSXMaster.board.on('down', Circle.down);
		JSXMaster.board.on('move', Circle.move);
		JSXMaster.board.on('up', Circle.up);
	},
	unbinding: function(){
		//Circle.isBinding = false;
		JSXMaster.board.off('down', Circle.down);
		JSXMaster.board.off('move', Circle.move);
		JSXMaster.board.off('up', Circle.up);
	},
	/**@shapeType: solid, dash, dot of the boundary of each shape**/
	setShapeType: function(boundaryType){
		
		var activeShape = JSXMaster.getActiveShape();
		if(activeShape !== undefined){
			var activeShapeId = activeShape.activeShapeId;
			//if(Line.activeLineId !== undefined){
			//if(activeShape.cls.hasOwnProperty('circles'))
			var c = Circle.circles[activeShapeId];
			//var activeLine = JSXMaster.board.objects[Line.activeLineId];
			c.setAttribute({dash: boundaryType});
		}else{
			Circle.circleStyle.dash = boundaryType;
		}
	},
	/**@width: strokeWidth of shape**/
	setStrokeWidth: function(width){
		var id = JSXMaster.getActiveShape().activeShapeId,
			c = Circle.circles[id];
		c.setAttribute({strokeWidth: width});
	},
	/**@color: color of shape**/
	setShapeColor: function(color){
		var id = JSXMaster.getActiveShape().activeShapeId,
			c = Circle.circles[id];
		c.setAttribute({strokeColor: color});
	}
};
var Line = {
	line: undefined,
	currMousePosition : undefined,
	lines: {},
	activeLineId: undefined,
	options:{ 
		straightFirst:false, 
		straightLast:false, 
		strokeWidth:2,
		dash:0,
		strokeColor: JSXMaster.fakePoint.getAttributes().fillcolor
	},
	removeShape : function(lineId){
		//TODO
		var line = Line.lines[lineId];
		var p1 = line.point1, 
			p2 = line.point2;
		gui.deleteObj(line);
		gui.deleteObj(p1);
		gui.deleteObj(p2);
		delete Line.lines[lineId];
	
	},
	/**@shapeType: solid, dash, dot of the boundary of each shape**/
	setShapeType: function(lineType){
		if(Line.activeLineId !== undefined){
			var activeLine = JSXMaster.board.objects[Line.activeLineId];
			activeLine.setAttribute({dash: lineType});
		}else{
			Line.options.dash = lineType;
		}
	},
	setStrokeWidth: function(width){
		if(Line.activeLineId !== undefined){
			var activeLine = JSXMaster.board.objects[Line.activeLineId];
			//var w = parseInt(width);
			activeLine.setAttribute({strokeWidth: width});
		}else{
			//Line.options.strokeWidth = parseInt(gui.LineStrokeWidth());
			Line.options.strokeWidth = width;
		}
	},
	setShapeColor: function(color){
		if(Line.activeLineId !== undefined){
			var activeLine = JSXMaster.board.objects[Line.activeLineId];
			activeLine.setAttribute({strokeColor: color});
		}else{
			Line.options.strokeColor = color;
		}
	},
	down: function(e){
		var first, coords = JSXMaster.getMouseCoords(e, 0);
		//console.log('down');
		/*
		for (el in JSXMaster.board.objects) {
			
			if(el !== JSXMaster.fakePoint.id && JXG.isPoint(JSXMaster.board.objects[el]) 
				&& JSXMaster.board.objects[el].hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
				
				shouldCreate = false;
				first = JSXMaster.board.objects[el];
				break;
			}
		}
		*/
		//console.log("mouse down");
		
		if(Line.line !== undefined && 
			Line.line.point1.hasPoint(coords.scrCoords[1], coords.scrCoords[2])){
			return;
		}
		first = JSXMaster.board.create('point', JSXMaster.currentMouseCoords, JSXMaster.pointStyle);
		
		if(Line.line !== undefined){
			Line.line.point2 = first;
			//Line.hideVertices(Line.line.id);
			Line.bindEventForLine(Line.line);
			Line.activateShape(Line.line.id);
			Line.line = undefined;
			Line.activeLineId = undefined;
			return;
		}
		var options = Line.options;
		Line.line = JSXMaster.board.createElement('line',[first, JSXMaster.fakePoint], options);
	
		//Line.bindEventForLine(Line.line);
		JSXMaster.deactivateExcept(Line.line.id);
		JSXMaster.showToolbar();
	},
	bindEventForLine: function(line){
		line.on('over', Line.overLine);
		line.on('out', Line.outLine);
		Line.lines[line.id] = line;
		line.on('drag', Line.draggingLine);
		line.on('down', Line.clickingLine);
	},
	bindEventForLine2: function(line){
		line.on('over', Line.overLine);
		line.on('out', Line.outLine);
		Line.lines[line.id] = line;
		//line.on('drag', Line.draggingLine);
		line.on('down', Line.clickingLine);
	},
	draggingLine: function(e){
		//http://jsxgraph.uni-bayreuth.de/docs/symbols/JXG.GeometryElement.html#event:drag
		var self = this;
		
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;

		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var first = this.point1, 
			second = this.point2;
		//first.prepareUpdate().update().updateRenderer();
		second.prepareUpdate().update().updateRenderer();
		
		second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);
		var mid = [(first.X() + second.X())/2 + vector.x, (first.Y() + second.Y())/2 + vector.y];
		JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
		JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
		//JSXMaster.fakePoint.showElement();
		JSXMaster.fakeText.hideElement();
	},
	clickingLine: function(e){
		//console.log("click edge");
		Line.activeLineId = this.id;
		//console.log(Line.activeLineId);
		JSXMaster.getMouseCoords(e, 0); 
		JSXMaster.deactivateExcept(this.id);
		
		var attrs = this.getAttributes();
		

		gui.ActivateItem('line');
		Line.activateShape(this.id);
		gui.LineStrokeWidth(attrs.strokewidth);
		gui.LineColorValue(attrs.strokecolor);
		document.getElementById(gui.LineColorInputId()).color.fromString(gui.LineColorValue());
		switch(attrs.dash){
			case 0:
				
				gui.DashActive(""); 
				gui.DotActive("");
				gui.SolidActive("toolbar_item_click");
				break;
			case 1:
				gui.DashActive(""); 
				gui.DotActive("toolbar_item_click");
				gui.SolidActive("");
				break;
			case 2:
				gui.DashActive("toolbar_item_click"); 
				gui.DotActive("");
				gui.SolidActive("");
				break;
			default:
				break;
		}
		
	},
	overLine : function(e){
		//console.log("on element");
		var self = this;
		JSXMaster.disableBoardEvent();
		//JSXMaster.board.off('down', Line.down);
		//console.log(JSON.stringify(self.attrs));
		//self.withLabel = true;
		JSXMaster.board.containerObj.style.cursor = 'move';
		//JSXMaster.fakePoint.hideElement();
		JSXMaster.hideFakePnt();
		var p1 = self.point1, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X())/2, (p1.Y() + p2.Y())/2];
		sum = v[0] * v[0] + v[1] * v[1];
		var dist = Math.sqrt(sum);
		dist = Math.round(dist*100)/100;
		//alert(dist);
		JSXMaster.fakeText.setText( "<strong>"+dist+"</strong>");
		//console.log(JSON.stringify(mid));
		JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
		JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
		JSXMaster.fakeText.showElement();
		JSXMaster.board.fullUpdate();
	},
	outLine: function(e){
		//console.log("out element");
		var self = this;
		//if(JSXMaster.SHAPE_MODE === JSXMaster.SHAPING_MODES.LINE && JSXMaster.board.eventHandlers.down === undefined)
		//	JSXMaster.board.on('down', Line.down);
		JSXMaster.enableBoardEvent();
		//self.withLabel = false;
		//JSXMaster.fakePoint.showElement();
		JSXMaster.showFakePnt();
		JSXMaster.fakeText.hideElement();
		//JSXMaster.board.containerObj.style.cursor = 'default';
	},
	restoreStyle: function(){
		console.log('restore');
		JSXMaster.board.containerObj.style.cursor = 'default';
	},
	move: function(e){
		//console.log('mouse move');
		JSXMaster.getMouseCoords(e, 0);
		JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, JSXMaster.currentMouseCoords);
		//JSXMaster.fakePoint.getTextAnchor.update().updateRenderer();
		JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
		if(Line.line !== undefined){
			Line.line.prepareUpdate().update().updateRenderer();
		}
		
		//getTextAnchor
		
	},
	showVertices: function(cId){
		var l = Line.lines[cId];
		l.point1.showElement();
		l.point2.showElement();
	},
	hideVertices: function(cId){
		var l = Line.lines[cId];
		l.point1.hideElement();
		l.point2.hideElement();
	},
	activateShape: function(lineId){
		JSXMaster.setActiveShape(Line, lineId, 'line');
		Line.showVertices(lineId);
	},
	deactivateShape: function(lineId){
		Line.hideVertices(lineId);
	},
	showHideToolbar: function(action, type){
		if(action === 'hide'){
			gui.ActivateItem('');
		}else{
			(type === undefined) ? type = 'line' : type = type;
			gui.ActivateItem(type)
		}
	},
	up: function(e){
		
		//Circle.unbinding();
	},
	binding: function(){
		JSXMaster.board.on('down', Line.down);
		JSXMaster.board.on('move', Line.move);
		JSXMaster.board.on('up', Line.up);
	},
	unbinding: function(){
		JSXMaster.board.off('down', Line.down);
		JSXMaster.board.off('move', Line.move);
		JSXMaster.board.off('up', Line.up);
	}
};
var TextOBJ = {
	defaultStyle: {
		fontSize: 20,
		strokeColor: '#ff0000',
		display: 'html',
		isLabel: true,
		highlightCssClass: 'fonthighlight',
		cssClass: 'myFont'
	},
	texts: {},
	/**@structure: key: @textId, value: html input object that associates with @textId**/
	textInputs: {},
	/** the input html that is active currently **/
	activeBlankInput: undefined,
	/** active text object id **/
	activeTextId: undefined,
	/** the text object this is editing **/
	editingText: undefined,
	removeShape : function(textId){
		var self = TextOBJ;
		var txt = self.texts[textId];
		gui.deleteObj(txt);
		delete self.texts[textId];
		
	},
	addText: function(text, options, position){
		var self = TextOBJ;
		options = options || self.defaultStyle;
		//console.log("dkm:" + TextOBJ.defaultStyle);
		for(var p in self.defaultStyle){
			if(options[p] === undefined){
				//console.log(p);
				options[p] = self.defaultStyle[p];
			}
		}
		//console.log(options);
		(position === undefined) ? position = [0, 0, text] : position = [position[0], position[1], text];
		var txt = JSXMaster.board.create('text',position, options);
		//JSXMaster.setDrawingMode(self, 'text');
		self.bindEventForText(txt);
		TextOBJ.activateShape(txt.id);
		JSXMaster.deactivateExcept(txt.id);
		JSXMaster.showToolbar();
		self.showVertices(txt.id);
		return txt;
	},
	init: function(){
		var self = TextOBJ;
		self.activeTextId = undefined;
		JSXMaster.board.containerObj.style.cursor = 'text';
		self.binding();
	},
	restoreStyle: function(){
		console.log('restore properties of text');
		JSXMaster.board.containerObj.style.cursor = 'text';
	},
	down: function(e){
		//console.log('downtext');
		//JSXMaster.board.containerObj.style.cursor = 'text';
		var self = TextOBJ;
		if(self.activeBlankInput !== undefined){
			//There is one input is still active
			if(self.editingText !== undefined) self.editingText.showElement();
			self.activeBlankInput.remove();
			self.activeBlankInput = undefined;
		}
		JSXMaster.deactivateAll();
		var end = JSXMaster.board.getMousePosition(e, 0);
		end[0]= e.clientX;
		end[1]= e.clientY;
		var coords = JSXMaster.getMouseCoords(e, 0);
		//var c = 
		//for(var key in JSXMaster.board.objects)
		for(var key in self.texts){
			var txt = self.texts[key];
			if(txt.hasPoint(coords.scrCoords[1], coords.scrCoords[2])){
				return;
			}
		}
		//var changedE = e;
		//var end = JSXMaster.currentMouseCoords;
		//console.log(end[0] + " xxx " + end[1]);
		//console.log(window.innerWidth + " vs " + window.innerHeight);
		var ENTER_KEY_CODE = 13,
			ESC_KEY_CODE = 27;
		self.activeBlankInput = 
		
			jQuery('<input type="text" wrap="soft" class="fonthighlight"></input>')
			.css({
				top: (end[1] - 10) + 'px',
				left: end[0] + 'px',
				width: '100px',
				height: '20px',
				'font-size': self.defaultStyle.fontSize + 'px',
				'line-height': '150%',
				'color': self.defaultStyle.strokeColor,
				'background-color': '#ffffff',
				//'margin': '100px',
				'border-radius': 5 + 'px',
				'overflow': 'hidden',
				'position': 'absolute'
				
			})
			.appendTo("body")
			.keydown(function (e) {
				if (e.shiftKey && e.which === ENTER_KEY_CODE) {
					return; // allow shift+enter to break lines
				}
				else if (e.which === ENTER_KEY_CODE) {
					//onCommit();
					//add text 
					console.log('it run');
					var options = {'fontSize' : self.activeBlankInput.css('font-size'), 'strokeColor': self.activeBlankInput.css('color')};
				//	var pos = [ideaInput.css('left'), ideaInput.css('top')];
					
					var pos = [coords.usrCoords[1], coords.usrCoords[2]];
					//console.log(JSON.stringify(e));
					
					var txt = self.addText(self.activeBlankInput.val(), options, pos);
					self.activeBlankInput.css('display', 'none'); 
					self.textInputs[txt.id] = self.activeBlankInput;
					//self.activeBlankInput.remove();
					self.activeBlankInput = undefined;
				} else if (e.which === ESC_KEY_CODE) {
					//onCancelEdit();
				} else if (e.which === 9) {
					//onCommit();
					e.preventDefault();
					//self.fire(':request', { type: 'addSubIdea', source: 'keyboard' });
					return;
				} else if (e.which === 83 && (e.metaKey || e.ctrlKey)) {
					e.preventDefault();
					//onCommit();
					return; /* propagate to let the environment handle ctrl+s */
				} else if (!e.shiftKey && e.which === 90 && (e.metaKey || e.ctrlKey)) {
					if (ideaInput.val() === self.unformattedText) {
					//	onCancelEdit();
					}
				}
				e.stopPropagation();
				})
				.focus(function(){
					//alert('fuck');
				}).dblclick(function(){
					//alert('hello');
					//this.css('display', 'block'); 
				});
			
	},
	clickText: function(e){
		TextOBJ.activeTextId = this.id;
		//console.log('strokecolor:' + )
		gui.TextColorValue(this.getAttributes().strokecolor);
		gui.TextStrokeWidth(this.getAttributes().fontsize);
		document.getElementById(gui.TextColorInputId()).color.fromString(gui.TextColorValue());
		gui.ActivateItem('text');
		JSXMaster.deactivateExcept(this.id);
		TextOBJ.activateShape(this.id);
	},
	overText: function(e){
		JSXMaster.disableBoardEvent();
		gui.showHideFakePoint('hide');
		JSXMaster.board.containerObj.style.cursor = 'move';
	},
	outText: function(e){
		JSXMaster.enableBoardEvent();
		gui.showHideFakePoint('show');
		//JSXMaster.board.containerObj.style.cursor = 'default';
	},
	dblClick: function(txt){
		JSXMaster.board.containerObj.addEventListener('dblclick', function(e){
			console.log('dblClick:' + JSON.stringify(e));
			var self = TextOBJ, 
				shape = JSXMaster.getActiveShape(),
				ENTER_KEY_CODE = 13,
				ESC_KEY_CODE = 27;
			if(self.editingText !== undefined){
				var id = self.editingText.id,
					input = self.textInputs[id];
				input.css('display', 'none');
				self.editingText.showElement();
			}
			if(shape.type === 'text'){
				var txtInput = TextOBJ.textInputs[shape.activeShapeId];
				if(txtInput !== undefined){
					var end = JSXMaster.board.getMousePosition(e, 0);
					var coords = JSXMaster.getMouseCoords(e, 0);
					end[0]= e.clientX;
					end[1]= e.clientY;
					txtInput.css('top', (end[1] - 15) + 'px');
					txtInput.css('left', (end[0] - 30) + 'px');
					//console.log('it works');
					txtInput.css('display', 'block');
					var textObj = shape.cls.texts[shape.activeShapeId];
					textObj.hideElement();
					//if(self.activeBlankInput !== undefined){
					//console.log('it is really good');
					//self.activeBlankInput.remove(); 
					//self.activeBlankInput = undefined;
					//}
					//self.activeBlankInput = txtInput;
					self.editingText = textObj;
					txtInput.unbind('keydown');
					txtInput.keydown(function (e) {
							if (e.shiftKey && e.which === ENTER_KEY_CODE) {
								return; // allow shift+enter to break lines
							}
							else if (e.which === ENTER_KEY_CODE) {
								var options = {'fontSize' : txtInput.css('font-size'), 'strokeColor': txtInput.css('color')};
								
								var pos = [coords.usrCoords[1], coords.usrCoords[2]];
								
								textObj.showElement();
								textObj.setText(txtInput.val());
								
								textObj.prepareUpdate().update().updateRenderer();
								textObj.setAttribute(options);
								txtInput.css('display', 'none'); 
								//self.activeBlankInput = undefined;
								self.editingText = undefined;
							} else if (e.which === ESC_KEY_CODE) {
								//onCancelEdit();
							} else if (e.which === 9) {
								//onCommit();
								e.preventDefault();
								//self.fire(':request', { type: 'addSubIdea', source: 'keyboard' });
								return;
							} else if (e.which === 83 && (e.metaKey || e.ctrlKey)) {
								e.preventDefault();
								//onCommit();
								return; /* propagate to let the environment handle ctrl+s */
							} else if (!e.shiftKey && e.which === 90 && (e.metaKey || e.ctrlKey)) {
								if (ideaInput.val() === self.unformattedText) {
								//	onCancelEdit();
								}
							}
							e.stopPropagation();
					});
					
				}
			}
		});
	},
	bindEventForText: function(text){
		TextOBJ.texts[text.id] = text;
		text.on('down', TextOBJ.clickText);
		text.on('over', TextOBJ.overText);
		text.on('out', TextOBJ.outText);
		TextOBJ.dblClick();
	},
	binding: function(){
		JSXMaster.board.on('down', TextOBJ.down);
	},
	unbinding: function(){
		var self = TextOBJ;
		JSXMaster.board.off('down', TextOBJ.down);
		if(self.activeBlankInput !== undefined){
			self.activeBlankInput.remove();
			self.activeBlankInput = undefined;
		}
	},
	showHideToolbar: function(action, type){
		if(action === 'hide'){
			gui.ActivateItem('');
		}else{
			(type === undefined) ? type = 'text' : type = type;
			gui.ActivateItem(type)
		}
	},
	showVertices: function(cId){
		var text = TextOBJ.texts[cId]
		text.setAttribute({cssClass: 'fonthighlight'});
		//text.showElement('cssClass', );
	},
	hideVertices: function(cId){
		var text = TextOBJ.texts[cId]
		//text.hideElement();
		text.setAttribute({cssClass: 'myFont'});

	},
	activateShape: function(tId){
		JSXMaster.setActiveShape(TextOBJ, tId, 'text');
		TextOBJ.showVertices(tId);
	},
	deactivateShape: function(tId){
		TextOBJ.hideVertices(tId);
	}
};
var ImageOBJ = {
	styles: {},
	images: {},
	activeImageId: undefined,
	removeShape : function(imageId){
		//TODO
	},
	addImage: function(img, options){
		options = options || ImageOBJ.styles;		
		var w = img.width / JSXMaster.scale,
			h = img.height / JSXMaster.scale;

		var im = JSXMaster.board.create('image', [img.src, [-w/2,-h/2], [w,h]], options);
		ImageOBJ.bindEventForImage(im);
	},
	setBackground: function(img, options){
		options = options || ImageOBJ.styles;
		options['fixed'] = true;
		var w = img.width / JSXMaster.scale,
			h = img.height / JSXMaster.scale;
		//w = JSXMaster.mapWidth;
		//h = JSXMaster.mapHeight;
		var im = JSXMaster.board.create('image', [img.src, [-w/2,-h/2], [w,h]], options);		
		ImageOBJ.bindEventForImage(im);
	},
	addImage2: function(params, options){
		options = options || ImageOBJ.styles;					
		var im = JSXMaster.board.create('image', params, options);
		ImageOBJ.bindEventForImage(im);
	},
	init: function(){
		ImageOBJ.styles = {};
		//TextOBJ.activeTextId = undefined;
	},
	imageClicking: function(e){
		
	},
	bindEventForImage: function(img){
		ImageOBJ.images[img.id] = img;
		img.noHighlight = function(){} //override to disable highlight
		img.highlight = function(){}
	},
	binding: function(){
		
	},
	showVertices: function(cId){
		var img = JSXMaster.board.objects[cId];
		img.showElement();
	},
	hideVertices: function(cId){
		var img = JSXMaster.board.objects[cId];
		img.hideElement();
	},
	showHideToolbar: function(action, type){
		if(action === 'hide'){
			gui.ActivateItem('');
		}else{
			(type === undefined) ? type = 'image' : type = type;
			gui.ActivateItem(type)
		}
	},
	activateShape: function(imgId){
		JSXMaster.setActiveShape(ImageOBJ, imgId, 'image');
		ImageOBJ.showVertices(imgId);
	},
	deactivateShape: function(imgId){
		ImageOBJ.hideVertices(imgId);
	}
};
var Rectangle = {
	rectangle: undefined,
	//lines:[],
	//points:[],
	prefix: "rect",
	counter: 0,
	currRectId: undefined,
	/**
	@field: rects
	@type: dictionary of rectangles drawn on the screen.
	@structure: key: rectangleId, value: an array of vertices belonging to @rectangleId 
	@aim: many things. 
	@example: {rect1: [v1, v2,...,vn], rect2: [], ....}
	*/
	rects: {}, 
	
	/**
	@field: lines
	@structure: key: rectangleId, value: an array of edges belonging to @rectangleId
	@type: dictionary of edges of a rectangle.
	@aim: the set of lines belonging to the current rect. This set is used only for binding events.
	*/
	lines: {}, 
	
	/**
	@field: mappingPoints 
	@type: dictionary
	@structure: key: pointID, value: rectangleID. 
	@aim: This dictionary is used for lookup a rectangle that a vertex belongs to.
	*/
	mappingPoints: {},
	init: function(){
		Rectangle.mappingPoints = {};
		Rectangle.lines = {};
		Rectangle.rects = {};
		Rectangle.counter = 0;
		Rectangle.currRectId = undefined;
	},
	removeShape : function(rectId){
		//TODO
		var pts = Rectangle.rects[rectId];
		var lines = Rectangle.lines[rectId];
		for(var i=0; i<lines.length; i++){
			gui.deleteObj(lines[i]);
		}
		for(var i=0; i<pts.length; i++){
			delete Rectangle.mappingPoints[pts[i].id];
			gui.deleteObj(pts[i]);
		}
		delete Rectangle.lines[rectId];
		delete Rectangle.rects[rectId];
	},
	/**@shapeType: solid, dash, dot of the boundary of each shape**/
	setShapeType: function(shapeType){
		//TODO
		var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Rectangle.lines[activeId];
		for(var i=0; i<shape.length; i++){
			var line = shape[i];
			line.setAttribute({dash: shapeType});
		}
		
	},
	/**@width: width of lines of rect**/
	setStrokeWidth: function(width){
		var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Rectangle.lines[activeId];
		for(var i=0; i<shape.length; i++){
			var line = shape[i];
			line.setAttribute({strokeWidth: width});
		}
	},
	/**@color: color of shape**/
	setShapeColor: function(color){
		var activeId = JSXMaster.getActiveShape().activeShapeId,
			shape = Rectangle.lines[activeId];
		for(var i=0; i<shape.length; i++){
			var line = shape[i];
			line.setAttribute({strokeColor: color});
		}
	},
	down: function(e){
		var coords, first;
		
		coords = JSXMaster.getMouseCoords(e, 0);
		first = JSXMaster.board.create('point', [coords.usrCoords[1], coords.usrCoords[2]], JSXMaster.pointStyle);

		Rectangle.currRectId = Rectangle.prefix + "_" + Rectangle.counter;
		var rectId = Rectangle.currRectId;
		//alert(rectId);
		JSXMaster.deactivateExcept(rectId);
		JSXMaster.showToolbar();
		
		if(Rectangle.rectangle !== undefined){
			
			Rectangle.completeRect(rectId, first);
			//Rectangle.hideVertices(rectId);
			Rectangle.activateShape(rectId);
			Rectangle.initHoverBinding(rectId);
			Rectangle.initDraggingListener(rectId, true);
			return;
		}

		var p3 = {x: first.X(), y: first.Y()};
		var secondPoint = JSXMaster.board.create('point', [function(){return JSXMaster.fakePoint.X();}, p3.y], JSXMaster.pointStyle);
		
		var fourthPoint = JSXMaster.board.create('point', [p3.x, function(){return JSXMaster.fakePoint.Y();}], JSXMaster.pointStyle);
		
		Rectangle.rects[rectId] = Rectangle.rects[rectId] || [];
		Rectangle.rects[rectId].push(first);
		Rectangle.rects[rectId].push(secondPoint);
		Rectangle.rects[rectId].push(JSXMaster.fakePoint);
		Rectangle.rects[rectId].push(fourthPoint);
		
		var points = Rectangle.rects[rectId];
		var options = { straightFirst:false, straightLast:false, strokeWidth:2};
		var firstLine = JSXMaster.board.createElement('line',[points[0], points[1]], options);	
			
		var secondLine = JSXMaster.board.createElement('line',[points[1], points[2]], options);		
			
		var thirdLine = JSXMaster.board.createElement('line',[points[3], points[2]], options);	
			
		var fourthLine = JSXMaster.board.createElement('line',[points[3], points[0]], options);	
			
		Rectangle.lines[rectId] = Rectangle.lines[rectId] || [];
		Rectangle.lines[rectId].push(firstLine);
		Rectangle.lines[rectId].push(secondLine);
		Rectangle.lines[rectId].push(thirdLine);
		Rectangle.lines[rectId].push(fourthLine);

		Rectangle.rectangle = "created";

	},
	initHoverBinding: function(rectId){
		var lines = Rectangle.lines[rectId] || [];
		for(var i=0; i<lines.length; i++){
			lines[i].on('over', Rectangle.overLine);
			lines[i].on('out', Rectangle.outLine);
		}
		
	},
	overLine : function(e){
		//console.log("on element");
		var self = this;
		//console.log(JSON.stringify(self.attrs));
		//self.withLabel = true;
		JSXMaster.disableBoardEvent();
		JSXMaster.board.containerObj.style.cursor = 'move';
		//JSXMaster.fakePoint.hideElement();
		JSXMaster.hideFakePnt();
		var p1 = self.point1, p2 = self.point2, sum = 0,
			v = [p1.X() - p2.X(), p1.Y() - p2.Y()], mid = [(p1.X() + p2.X())/2, (p1.Y() + p2.Y())/2];
		sum = v[0] * v[0] + v[1] * v[1];
		var dist = Math.sqrt(sum);
		dist = Math.round(dist*100)/100;
		//alert(dist);
		JSXMaster.fakeText.setText( "<strong>"+dist+"</strong>");
		//console.log(JSON.stringify(mid));
		JSXMaster.fakeText.setPosition(JXG.COORDS_BY_USER, mid);
		JSXMaster.fakeText.prepareUpdate().update().updateRenderer();
		JSXMaster.fakeText.showElement();
		JSXMaster.board.fullUpdate();
	},
	outLine: function(e){
		//console.log("out element");
		var self = this;
		//self.withLabel = false;
		JSXMaster.enableBoardEvent();
		//JSXMaster.board.containerObj.style.cursor = 'default';
		//JSXMaster.fakePoint.showElement();
		JSXMaster.showFakePnt();
		JSXMaster.fakeText.hideElement();
	},
	restoreStyle: function(){
		console.log('restore');
		JSXMaster.board.containerObj.style.cursor = 'default';
	},
	completeRect : function(rectId, lastpoint){
		
		//reset, very important
		Rectangle.rectangle = undefined;
			//Rectangle.secondPoint.addConstraint([x, y]);
		var points = Rectangle.rects[rectId] || [];
		points[1].free();
		points[3].free();
		points[2] = lastpoint;
		for(var i=0; i<points.length; i++){
			Rectangle.mappingPoints[points[i].id] = rectId;
		}
		var lines = Rectangle.lines[rectId] || [];
		lines[1].point2 = lastpoint;
		lines[2].point2 = lastpoint;
		Rectangle.counter++; //increase id of distinct rect
	},
	initDraggingListener: function(rectId, realTime){
		/*
		var down = function(e){
			JSXMaster.getMouseCoords(e, 0);
			gui.ActivateItem('rectangle');
		}*/
		var lines = Rectangle.lines[rectId] || [];
		for(var i=0; i<lines.length; i++){
			lines[i].on('down', Rectangle.mouseDownEdge);
		}
		lines[0].on('drag', Rectangle.dragging1);
		if(realTime === undefined || realTime === false){
			lines[1].on('drag', Rectangle.dragging3);
			lines[2].on('drag', Rectangle.dragging3);
		}else{
			//real time
			lines[1].on('drag', Rectangle.dragging2);
			lines[2].on('drag', Rectangle.dragging2);
		}
		lines[3].on('drag', Rectangle.dragging1);
	},
	showVertices: function(polyId){
		//show all vertices when being active
		var pts = Rectangle.rects[polyId];
		for(var i=0; i<pts.length; i++){
			var p = pts[i];
			p.showElement();
		}
	},
	hideVertices: function(polyId){
		//hide all vertices, when being inactive
		//show all vertices when being active
		var pts = Rectangle.rects[polyId];
		for(var i=0; i<pts.length; i++){
			var p = pts[i];
			p.hideElement();
		}
	},
	mouseDownEdge: function(e){
		gui.ActivateItem('rectangle');
		JSXMaster.getMouseCoords(e, 0);
		var self = this, 
			rectID = Rectangle.mappingPoints[self.point1.id];
		Rectangle.setToolBar(rectID);
		JSXMaster.deactivateExcept(rectID);
		Rectangle.activateShape(rectID);
	},
	setToolBar: function(polyId){
		var attrs = Rectangle.lines[polyId][0].getAttributes();
		gui.DotActive("");
		gui.SolidActive("");
		gui.DashActive(""); 
		if(attrs.dash === 2){
			gui.DashActive("toolbar_item_click"); 
		}else if(attrs.dash === 1){
			gui.DotActive("toolbar_item_click");
		}else if(attrs.dash === 0){
			gui.SolidActive("toolbar_item_click");
		}
		gui.RectStrokeWidth(attrs.strokewidth);
		gui.RectColorValue(attrs.strokecolor);
		document.getElementById(gui.RectColorInputId()).color.fromString(gui.RectColorValue());
	},
	showHideToolbar: function(action, type){
		if(action === 'hide'){
			gui.ActivateItem('');
		}else{
			(type === undefined) ? type = 'rectangle' : type = type;
			gui.ActivateItem(type)
		}
	},
	activateShape: function(rectId){	
		JSXMaster.setActiveShape(Rectangle, rectId, 'rectangle');
		Rectangle.showVertices(rectId);
	},
	deactivateShape: function(rectId){
		Rectangle.hideVertices(rectId);
	},
	dragging3: function(e){
		//for un-real time
		var self = this;
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;
		
		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var first = self.point1, second = self.point2;
		JSXMaster.fakeText.hideElement();
		//second.prepareUpdate().update().updateRenderer();
		//second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);

		//first.prepareUpdate().update().updateRenderer();
		var rectId = Rectangle.mappingPoints[first.id];
		var points = Rectangle.rects[rectId] || [];
		for(var i=0; i<points.length; i++){
			var p = points[i];
			if(p.id !== first.id && p.id !== second.id){
				p.setPosition(JXG.COORDS_BY_USER, [p.X() + vector.x, p.Y() + vector.y]);
				p.prepareUpdate().update().updateRenderer();
			}
		}
	},
	dragging2: function(e){
		var self = this;
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;
		
		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var first = self.point1, second = self.point2;
		JSXMaster.fakeText.hideElement();
		second.prepareUpdate().update().updateRenderer();
		second.setPosition(JXG.COORDS_BY_USER, [second.X() + vector.x, second.Y() + vector.y]);

		//first.prepareUpdate().update().updateRenderer();
		var rectId = Rectangle.mappingPoints[first.id];
		var points = Rectangle.rects[rectId] || [];
		for(var i=0; i<points.length; i++){
			var p = points[i];
			if(p.id !== first.id && p.id !== second.id){
				p.setPosition(JXG.COORDS_BY_USER, [p.X() + vector.x, p.Y() + vector.y]);
				p.prepareUpdate().update().updateRenderer();
			}
		}
	},
	dragging1 : function(e){
		var self = this;
		var begin = JSXMaster.currentMouseCoords; //the position of mouse before dragging. 
		JSXMaster.getMouseCoords(e, 0); //this function will update mouse coords when begin to drag
		var end = JSXMaster.currentMouseCoords;
		JSXMaster.fakeText.hideElement();
		var vector = {x: end[0] - begin[0], y: end[1] - begin[1]};
		var first = self.point1, second = self.point2;
		
		var rectId = Rectangle.mappingPoints[first.id];
		var points = Rectangle.rects[rectId] || [];
		for(var i=0; i<points.length; i++){
			var p = points[i];
			if(p.id !== first.id && p.id !== second.id){
				
				p.prepareUpdate().update().updateRenderer();
				p.setPosition(JXG.COORDS_BY_USER, [p.X() + vector.x, p.Y() + vector.y]);
			}
		}
	},
	move: function(e){
		var coords = JSXMaster.getMouseCoords(e, 0);
		JSXMaster.fakePoint.setPosition(JXG.COORDS_BY_USER, coords.usrCoords.slice(1));
		
		JSXMaster.fakePoint.prepareUpdate().update().updateRenderer();
		var rectId = Rectangle.currRectId;
		if(rectId === undefined){
			return;
		}
		
		var points = Rectangle.rects[rectId] || [];
		var lines = Rectangle.lines[rectId] || [];
		
		for(var i=0; i<points.length; i++){
			var point = points[i];
			if(point !== undefined)
				point.prepareUpdate().update().updateRenderer();
		}
		for(var i=0; i<lines.length; i++){
			var line = lines[i];
			line.prepareUpdate().update().updateRenderer();
		}
		
	},
	up: function(e){
		//Circle.unbinding();
	},
	binding: function(){
		JSXMaster.board.on('down', Rectangle.down);
		JSXMaster.board.on('move', Rectangle.move);
		JSXMaster.board.on('up', Rectangle.up);
	},
	unbinding: function(){
		JSXMaster.board.off('down', Rectangle.down);
		JSXMaster.board.off('move', Rectangle.move);
		JSXMaster.board.off('up', Rectangle.up);
	}
};
DRAG = {
	down: function(e){
		var self = DRAG;
		var coords = JSXMaster.getMouseCoords(e);
		var shouldDisable = true;
		for(var id in JSXMaster.board.objects){
			var ele = JSXMaster.board.objects[id];
			if(id !== JSXMaster.fakePoint.id && ele.hasPoint(coords.scrCoords[1], coords.scrCoords[2])) {
				shouldDisable = false;
				break;
			}
		}
		if(shouldDisable){
			console.log('should do it');
			JSXMaster.deactivateAll();
			gui.ActivateItem('');
			self.restoreStyle();
		}
	},
	restoreStyle: function(){
		//console.log('restore');
		JSXMaster.board.containerObj.style.cursor = 'default';
	},
	binding: function(){
		JSXMaster.board.on('down', DRAG.down);
	},
	unbinding: function(){
		JSXMaster.board.off('down', DRAG.down);
	}
}
var GUI = function(){
	var self = this;
	self.Shapes = {};
	
	var inputIdname = "inputId";
	//var tempF = ko.observable();
	self.InputId = ko.observable(inputIdname);
	self.MapJSON = ko.observable("");
	self.map = undefined;
	self.init = function(){
		//alert("no");
		$(document).ready(function(){
			$('div.toolbar_item').hover(function(){
				//alert('shit');
				$(this).addClass('toolbar_item_hover');
			}, function(){
				$(this).removeClass('toolbar_item_hover');
			});
			self.InitImageEvent();
			self.InitImageEvent2();
			self.InitLoadFile();
			self.InitColorInput();
			self.InitTextColor();
			self.Shapes['line'] = Line;
			self.Shapes['polygon'] = Polygon;
			self.Shapes['rectangle'] = Rectangle;
			self.Shapes['circle'] = Circle;
			self.Shapes['text'] = TextOBJ;
			self.Shapes['image'] = ImageOBJ;
		});
		var keyDown = function (Evt) {
			var code;
			if (!Evt) Evt = window.event;
			if (Evt.which) {
				code = Evt.which;
			} else if (Evt.keyCode) {
				code = Evt.keyCode;
			}
			// 37: left,  38: up,  39: right,  40: down
			if (code==37) { self.ShiftLeft(); return false;}
			else if (code==38) { self.ShiftUp(); return false;}
			else if (code==39) { self.ShiftRight(); return false;}
			else if (code==40) { self.ShiftDown(); return false;}
			return true;
		}
		document.onkeydown = keyDown;
		
	};
	self.bindEvent = function(type){
		if(self.Shapes[type] !== undefined){
			var s = self.Shapes[type];
			s.binding();
		}
	}
	self.unbindEvent = function(type){
		if(self.Shapes[type] !== undefined){
			var s = self.Shapes[type];
			s.unbinding();
		}
	}
	self.bindOrUnbind = function(type){
		var status = self.ToolbarList[type];
		if(status === undefined) return;
		if(status() === 'none'){
			//show it
			self.showHideFakePoint('show');
			self.bindEvent(type);
		}else{
			self.showHideFakePoint('hide');
			self.unbindEvent(type);
		}
	}
	self.drawRect = function(){
		var status = self.ToolbarList['rectangle'];
		if(status() === 'block') { 
			self.Drag(); return;
		}
		self.drawShape('rectangle');
		JSXMaster.setDrawingMode(Rectangle, 'rectangle');
		JSXMaster.showFakePnt();
	}
	self.drawCircle = function(){
		var status = self.ToolbarList['circle'];
		if(status() === 'block') { 
			self.Drag(); return;
		}
		self.drawShape('circle');
		JSXMaster.setDrawingMode(Circle, 'circle');
		JSXMaster.showFakePnt();
	}
	self.drawPolygon = function(){
		var status = self.ToolbarList['polygon'];
		if(status() === 'block') { 
			self.Drag(); return;
		}
		self.drawShape('polygon');
		JSXMaster.setDrawingMode(Polygon, 'polygon');
		JSXMaster.showFakePnt();
	}
	self.DeleteShape = function(){
		JSXMaster.deleteActiveShape();
		JSXMaster.showToolbar();
		
	}
	self.drawShape = function(type){
		self.resetBinding();
		self.bindOrUnbind(type);
		self.ShowOnOffBar(type);
		//JSXMaster.deactivateAll();
		
	}
	
	/**unbind all events.**/
	self.resetBinding = function(){
		Polygon.unbinding();
		Circle.unbinding();
		Line.unbinding();
		Rectangle.unbinding();
		TextOBJ.unbinding();
		DRAG.unbinding();
		self.DeactivateShape();
		self.showHideFakePoint('hide');
		JSXMaster.setDrawingMode(undefined);
		JSXMaster.board.containerObj.style.cursor = 'default';
		//JSXMaster.SHAPE_MODE = undefined;
	}
	self.DeactivateShape = function(){
		//alert('Drag deactivate');
		JSXMaster.deactivateAll();
	}
	self.bindDragMode = function(){
		DRAG.binding();
	}
	self.Drag = function(){
		//self.DeactivateShape();
		self.resetBinding();
		self.bindDragMode();
		self.ActivateItem('drag');
		//JSXMaster.setDrawingMode(DRAG, 'drag');
	}
	self.reInitMap = function(){
		Polygon.init();
		Rectangle.init();
	}
	self.showHideFakePoint = function(type){
		if(type === undefined) type = 'hide';
		(type === 'show') ? JSXMaster.showFakePnt() : JSXMaster.hideFakePnt();
	}
	self.removeAll = function(){
		JXG.JSXGraph.freeBoard(JSXMaster.board);
		JSXMaster.init();
	}
	self.saveFile = function(){
		//alert("save file");
		var map = {},  
			points_new = [], 
			lines_new = [], 
			circles_new = [], 
			polygons_new = [], 
			rectangles_new = [],
			texts = [],
			images = [];
		//data structure for circles
		for(var circleId in Circle.circles){
			var obj = Circle.circles[circleId];
			var circle = {id: obj.id, center: obj.center.id, point2: obj.point2.id, attrs: obj.getAttributes()};
			circles_new.push(circle);
		}
		//data structure for polygons
		for(var polygonId in Polygon.lines){
			var lines = Polygon.lines[polygonId];
			var edges = [];
			
			for(var i=0; i<lines.length; i++){
				var first = lines[i].point1, second = lines[i].point2;
				edges.push({first: first.id, second: second.id, attrs: lines[i].getAttributes()});
			}
			
			var poly = {polygonId: polygonId, edges: edges};
			polygons_new.push(poly);
		}
		//create data structure for list of rectangles
		for(var rectId in Rectangle.rects){
			var lines = Rectangle.lines[rectId];
			var edges = [];
			
			for(var i=0; i<lines.length; i++){
				var first = lines[i].point1, second = lines[i].point2;
				edges.push({first: first.id, second: second.id, attrs: lines[i].getAttributes()});
			}
			
			var rect = {rectId: rectId, edges: edges};
			rectangles_new.push(rect);
		}
		//data structure for lines
		for(var lineId in Line.lines){
			//alert(lineId);
			var obj = Line.lines[lineId];
			var line = {id: obj.id, point1: obj.point1.id, point2: obj.point2.id, attrs: obj.getAttributes()};
			lines_new.push(line);		
		}
		for(var textId in TextOBJ.texts){
			var obj = TextOBJ.texts[textId];
			var txt = {id: obj.id, x: obj.X(), y: obj.Y(), attrs: obj.getAttributes(), content: obj.plaintext};
			texts.push(txt);
		}
		for(var imgId in ImageOBJ.images){
			var obj = ImageOBJ.images[imgId];
			var im = {id: obj.id, coords: [obj.X(), obj.Y()], dims: obj.size, attrs: obj.getAttributes(), url: obj.url};
			images.push(im);
		}
		for (var el in JSXMaster.board.objects) {
			var obj = JSXMaster.board.objects[el];
			console.log(obj.getType());
			if(obj.getType() === 'point' && el !== JSXMaster.fakePoint.id){
				var point = {id: obj.id, x: obj.X(), y: obj.Y(), attrs: obj.getAttributes()};
				points_new.push(point);
			}else if(obj.getType() === 'text'){
				
			}else if(obj.getType() === 'image'){
				//var img = {id: obj.id, x: obj.X(), y: obj.Y(), attrs: obj.getAttributes()};
				//images.push(txt);
			}
			
		}
		
		map["points"] = points_new;
		map["lines"] = lines_new;
		map["circles"] = circles_new;
		map["polygons"] = polygons_new;
		map["rectangles"] = rectangles_new;
		map["texts"] = texts;
		map['images'] = images;
		//alert(JSON.stringify(lines_new));
		var result = JSON.stringify(map);
		var blob = new Blob([result], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "drawing.txt");
	}
	self.drawMap = function(map){
		
		self.removeAll();
		self.ActivateItem('');
		self.resetBinding();
		self.reInitMap();
		//alert(map.points.length);
		for(var i=0; i<map.points.length; i++){
			//alert("point la: " + JSON.stringify(map.points[i]));
			var point = map.points[i];
			var p = JSXMaster.board.create('point', [point.x, point.y], point.attrs);
			p.id = point.id;
		}
		
		
		for(var i=0; i<map.lines.length; i++){
			var line = map.lines[i];
			var p1 = JSXMaster.board.objects[line.point1];
			var p2 = JSXMaster.board.objects[line.point2];
			var l = JSXMaster.board.create('line', [p2, p1], line.attrs);
			l.id = line.id;
			Line.bindEventForLine2(l);
		}
		
		for(var i=0; i<map.circles.length; i++){
			var circle = map.circles[i];
			//alert("hell");
			var center = JSXMaster.board.objects[circle.center];
			var point2 = JSXMaster.board.objects[circle.point2];
			//alert("center la: " + center);
			//alert("point2 la: " + point2);
			var c = JSXMaster.board.create('circle', [center, point2], circle.attrs);
			c.id = circle.id;
			Circle.bindEventForCircle2(c);
		}
		for(var i=0; i<map.polygons.length; i++){
			var poly = map.polygons[i];
			//alert(JSON.stringify(poly));	
			var polygonId = poly.polygonId, edges = poly.edges;
			for(var j=0; j<edges.length; j++){
				var edge = edges[j];
				var f = edge.first, 
					s = edge.second, 
					attrs = edge.attrs;
				//alert(f + " " + s);
			
				var p1 = JSXMaster.board.objects[f];
				var p2 = JSXMaster.board.objects[s];
				var l = JSXMaster.board.create('line', [p1, p2], attrs);
				
				//----- Add line.
				Polygon.lines[polygonId] = Polygon.lines[polygonId] || [];
				Polygon.lines[polygonId].push(l);
				//-----Add points and mapping Points
				Polygon.polygons[polygonId] = Polygon.polygons[polygonId] || [];
				if(Polygon.mappingPoints[f] === undefined){
					Polygon.polygons[polygonId].push(p1);
					Polygon.mappingPoints[f] = polygonId;
				}
				if(Polygon.mappingPoints[s] === undefined){
					Polygon.polygons[polygonId].push(p2);
					Polygon.mappingPoints[s] = polygonId;
				}
				
				//l.on('over', Polygon.overLine);
				//l.on('out', Polygon.outLine);
			}
			Polygon.initDraggingListener(polygonId, false);
			Polygon.initHoverBinding(polygonId);
			Polygon.counter++;
		}
		for(var i=0; i<map.rectangles.length; i++){
			var rect = map.rectangles[i];
			var rectId = rect.rectId, edges = rect.edges, f = edges.first, s = edges.second, attrs = edges.attrs;
			
			for(var j=0; j<edges.length; j++){
				var edge = edges[j];
				var f = edge.first, 
					s = edge.second, 
					attrs = edge.attrs;
				//alert(f + " " + s);
				//Rectangle.mappingPoints[f] = rectId;
				//Rectangle.mappingPoints[s] = rectId;
				var p1 = JSXMaster.board.objects[f];
				var p2 = JSXMaster.board.objects[s];
				var l = JSXMaster.board.create('line', [p1, p2], attrs);
				
				//----- Add line.
				Rectangle.lines[rectId] = Rectangle.lines[rectId] || [];
				Rectangle.lines[rectId].push(l);
				//-----Add points and mapping Points
				Rectangle.rects[rectId] = Rectangle.rects[rectId] || [];
				if(Rectangle.mappingPoints[f] === undefined){
					Rectangle.rects[rectId].push(p1);
					Rectangle.mappingPoints[f] = rectId;
				}
				if(Rectangle.mappingPoints[s] === undefined){
					Rectangle.rects[rectId].push(p2);
					Rectangle.mappingPoints[s] = rectId;
				}
				
			}
			
			//var p1 = JSXMaster.board.objects[f];
			//var p2 = JSXMaster.board.objects[s];
			//var l = JSXMaster.board.create('line', [p1, p2], attrs);
			Rectangle.initHoverBinding(rectId);
			Rectangle.initDraggingListener(rectId, false);
			Rectangle.counter++;
		}
		for(var i=0; i<map.texts.length; i++){
			var txt = map.texts[i];
			var text = JSXMaster.board.create('text', [txt.x, txt.y, txt.content], txt.attrs);
			text.id = txt.id;
			TextOBJ.texts[text.id] = text;
		}
		for(var i=0; i<map.images.length; i++){
			var img = map.images[i];
			//alert(JSON.stringify([img.coords, img.dims]));
			var w = img.dims[0] / JSXMaster.scale,
				h = img.dims[1] / JSXMaster.scale;
			//ImageOBJ.setBackground(img);
			ImageOBJ.addImage2([img.url, img.coords, [w, h]], img.attrs);
			//var image = JSXMaster.board.create('image', [img.src, img.coords, img.dims], img.attrs);
			//image.id = img.id;
			//Image.images[image.id] = img;
		}
	}
	self.LoadFile = function(){
	   var elem = document.getElementById(self.InputId());
	   if(elem && document.createEvent) {
		  var evt = document.createEvent("MouseEvents");
		  evt.initEvent("click", true, false);
		  elem.dispatchEvent(evt);
	   }
	}
	self.InitLoadFile = function(){
		
		var fileInput = document.getElementById(self.InputId());
		var fileDisplayArea = "";
		fileInput.addEventListener('click', function(e){
			this.value = null;
		});
		fileInput.addEventListener('change', function(e) {
			//alert("change");
			var file = fileInput.files[0];
			var textType = /text.*/;

			if (file.type.match(textType)) {
				var reader = new FileReader();

				reader.onload = function(e) {
					fileDisplayArea = reader.result;
					//alert(fileDisplayArea);
					self.MapJSON(fileDisplayArea);
					self.map = JSON.parse(fileDisplayArea);
					//alert(JSON.stringify(self.map));
					self.drawMap(self.map);
				}

				reader.readAsText(file);	
			} else {
				fileDisplayArea = "File not supported!";
				alert(fileDisplayArea);
			}
		});
	}
	//-------------------------------------------------------------------------------------//
	self.ImageInputId = ko.observable("image_background");
	self.LoadImage = function(){
		//No Need
		//console.log('load image');
		var elem = document.getElementById(self.ImageInputId());
		if(elem && document.createEvent) {
		  var evt = document.createEvent("MouseEvents");
		  evt.initEvent("click", true, false);
		  elem.dispatchEvent(evt);
		}
	}
	self.InitImageEvent = function(){
		
		var fileInput = document.getElementById(self.ImageInputId());
		var fileDisplayArea = "";
		fileInput.addEventListener('click', function(e){
			this.value = null;
		});
		fileInput.addEventListener('change', function(e) {
			var file = fileInput.files[0];
			var imageType = /image.*/;
			//alert(JSON.stringify(file.path));
			if (file.type.match(imageType)) {
				//var im = JSXMaster.board.create('image', [file.path, [-3,-2], [3,3]]);
				var reader = new FileReader();
				var img = new Image;
				reader.onload = function(e) {
					fileDisplayArea = reader.result;
					img.src = reader.result;
					//var w = img.width / JSXMaster.scale,
					//	h = img.height / JSXMaster.scale;
					ImageOBJ.addImage(img);
					//var im = JSXMaster.board.create('image', [img.src, [-3,-2], [w, h]]);
				}
				reader.readAsDataURL(file)	
			} else {
				fileDisplayArea = "File not supported!";
				alert(fileDisplayArea);
			}
		});
	}
	//-------------------------------------------------------------------------------------//
	
	self.ImageInputId2 = ko.observable("img_background_full");
	self.LoadImage2 = function(){
		var elem = document.getElementById(self.ImageInputId2());
		if(elem && document.createEvent) {
		  var evt = document.createEvent("MouseEvents");
		  evt.initEvent("click", true, false);
		  elem.dispatchEvent(evt);
		}
	}
	self.InitImageEvent2 = function(){
		
		var fileInput = document.getElementById(self.ImageInputId2());
		var fileDisplayArea = "";
		fileInput.addEventListener('click', function(e){
			this.value = null;
		});
		fileInput.addEventListener('change', function(e) {
			var file = fileInput.files[0];
			var imageType = /image.*/;
			//alert(JSON.stringify(file.path));
			if (file.type.match(imageType)) {
				//var im = JSXMaster.board.create('image', [file.path, [-3,-2], [3,3]]);
				var reader = new FileReader();
				var img = new Image;
				reader.onload = function(e) {
					fileDisplayArea = reader.result;
					img.src = reader.result;
					ImageOBJ.setBackground(img);
				}
				reader.readAsDataURL(file)	
			} else {
				fileDisplayArea = "File not supported!";
				alert(fileDisplayArea);
			}
		});
	}
	
	//-------------------------------------------------------------------------------------//
	self.ZoomIn = function(){
		JSXMaster.board.zoomIn();
	}
	self.ZoomOut = function(){
		JSXMaster.board.zoomOut();
	}
	self.Zoom100 = function(){
		JSXMaster.board.zoom100();
	}
	//self.Toolbar_Item_Hover = ko.observable("");
	self.ZoomBarId = ko.observable("zooming_bar_menu4");
	self.ZoomBarStatus = ko.observable("none");
	self.ShowZoomingBar = function(){
		self.ShowOnOffBar('zoom');	
	}
	self.OriginX = ko.observable(JSXMaster.board.origin.scrCoords[1]);
	self.OriginY = ko.observable(JSXMaster.board.origin.scrCoords[2]);
	self.ShiftLeft = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x - x*0.1, y);
	}
	self.ShiftRight = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x + x*0.1, y);
	}
	self.ShiftCenter = function(){
		JSXMaster.board.moveOrigin(self.OriginX(), self.OriginY());
	}
	self.ShiftUp = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x, y + y * 0.1);
	}
	self.ShiftDown = function(){
		var x = JSXMaster.board.origin.scrCoords[1],
			y = JSXMaster.board.origin.scrCoords[2];
			
		JSXMaster.board.moveOrigin(x, y - y * 0.1);
	}
	//----------------Line bar -------------------------//
	self.LineBarId = ko.observable("line_bar_id");
	self.LineBarStatus = ko.observable("none");
	
	self.drawLine = function(){
		var status = self.ToolbarList['line'];
		if(status() === 'block') { 
			self.Drag(); return;
		}
		self.drawShape('line');
		JSXMaster.setDrawingMode(Line, 'line');
		JSXMaster.showFakePnt();
	}
	self.SolidActive = ko.observable("");
	self.DotActive = ko.observable("");
	self.DashActive = ko.observable("");
	self.setShapeType = function(shapeType, boundaryType){
		
		var dash = 0;
		if(boundaryType === 'dash'){
			dash = 2;
			gui.DashActive("toolbar_item_click"); 
			gui.DotActive("");
			gui.SolidActive("");
		}else if(boundaryType === 'dot'){
			dash = 1;
			gui.DashActive(""); 
			gui.DotActive("toolbar_item_click");
			gui.SolidActive("");
		}else if(boundaryType === 'solid'){
			dash = 0;
			gui.DashActive(""); 
			gui.DotActive("");
			gui.SolidActive("toolbar_item_click");
		}
		switch(shapeType){
			case 'line':
				Line.setShapeType(dash);
				break;
			case 'polygon':
				Polygon.setShapeType(dash);
				break;
			case 'rectangle':
				Rectangle.setShapeType(dash);
				break;
			case 'circle':
				Circle.setShapeType(dash);
				break;
			case 'text':
				break;
			case 'image':
				break;
			default:
				break;
		}
		
	}
	//---------------------------------------------------------------------------------
	self.LineStrokeWidth = ko.observable('5');
	self.LineColorInputId = ko.observable('lineColor');
	self.LineColorValue = ko.observable(JSXMaster.fakePoint.getAttributes().fillcolor);
	//---------------------------------------------------------------------------------
	self.CircleStrokeWidth = ko.observable('5');
	self.CircleColorInputId = ko.observable('circleColorId');
	self.CircleColorValue = ko.observable(JSXMaster.fakePoint.getAttributes().fillcolor);
	//---------------------------------------------------------------------------------
	self.PolygonStrokeWidth = ko.observable('5');
	self.PolygonColorInputId = ko.observable('polygonColorId');
	self.PolygonColorValue = ko.observable(JSXMaster.fakePoint.getAttributes().fillcolor);
	//---------------------------------------------------------------------------------
	self.RectStrokeWidth = ko.observable('5');
	self.RectColorInputId = ko.observable('rectColorId');
	self.RectColorValue = ko.observable(JSXMaster.fakePoint.getAttributes().fillcolor);
	//---------------------------------------------------------------------------------
	
	self.InitColorInput = function(){
		var lineInput = document.getElementById(self.LineColorInputId());
		if(lineInput !== null){
			lineInput.addEventListener('change', function(e){
				var color = '#' + this.color;
				Line.setShapeColor(color);
			});
		}
		//-----------------------------
		var circleInput = document.getElementById(self.CircleColorInputId());
		if(circleInput !== null){
			circleInput.addEventListener('change', function(e){
				var color = '#' + this.color;
				Circle.setShapeColor(color);
			});
		}
		//-----------------------------
		var polyInput = document.getElementById(self.PolygonColorInputId());
		if(polyInput !== null){
			polyInput.addEventListener('change', function(e){
				var color = '#' + this.color;
				Polygon.setShapeColor(color);
			});
		}
		//-----------------------------
		var rectInput = document.getElementById(self.RectColorInputId());
		if(rectInput !== null){
			rectInput.addEventListener('change', function(e){
				var color = '#' + this.color;
				Rectangle.setShapeColor(color);
			});
			
		}
	}
	/*
	self.LineStrokeWidthChanged = function(){
		//alert(self.LineStrokeWidth());
		if(Line.activeLineId !== undefined){
			var activeLine = JSXMaster.board.objects[Line.activeLineId];
			var w = parseInt(self.LineStrokeWidth());
			activeLine.setAttribute({strokeWidth: w});
		}else{
			Line.options.strokeWidth = parseInt(self.LineStrokeWidth());
		}
	}*/
	self.ShapeStrokeWidthChanged = function(shapeType){
		switch(shapeType){
			case 'line':
				//Line.setShapeType(boundaryType);
				var w = parseInt(self.LineStrokeWidth());
				Line.setStrokeWidth(w);
				break;
			case 'polygon':
				var w = parseInt(self.PolygonStrokeWidth());
				Polygon.setStrokeWidth(w);
				break;
			case 'rectangle':
				var w = parseInt(self.RectStrokeWidth());
				Rectangle.setStrokeWidth(w);
				break;
			case 'circle':
				var w = parseInt(self.CircleStrokeWidth());
				Circle.setStrokeWidth(w);
				break;
			case 'text':
				break;
			case 'image':
				break;
			default:
				break;
		}
	}
	
	//--------------------------------For text----------------------------------------//
	self.drawText = function(type){
		self.resetBinding();
		TextOBJ.init();
		
		TextOBJ.activeTextId = undefined;
		self.ShowOnOffBar('text');
		$('input#' + self.TextInputId()).focus();
		JSXMaster.setDrawingMode(TextOBJ, 'text');
		//JSXMaster.board.
	}
	self.FocusText = function(){
		self.resetBinding();
		TextOBJ.activeTextId = undefined;
		self.ActivateItem('text');
	}
	self.TextInputId = ko.observable("textInputId_temp");
	self.AddText = function(d, e){
		if(e.keyCode === 13){
			JSXMaster.setDrawingMode(TextOBJ, 'text');
			TextOBJ.activeTextId = undefined;
			var text = $('input#' + self.TextInputId()).val();
			$('input#' + self.TextInputId()).val('');
			TextOBJ.addText(text);
		}
		
	}
	self.TextBarId = ko.observable("textToolbarEdit");
	self.TextBarStatus = ko.observable("none");
	self.TextStrokeWidth = ko.observable(TextOBJ.defaultStyle.fontSize);
	self.TextStrokeWidthChanged = function(){
		//TODO
		if(TextOBJ.activeTextId !== undefined){
			var activeText = JSXMaster.board.objects[TextOBJ.activeTextId];
			var w = parseInt(self.TextStrokeWidth());
			activeText.setAttribute({fontSize: w});
		}else{
			TextOBJ.defaultStyle.fontSize = parseInt(self.TextStrokeWidth());
		}
	}
	self.TextColorInputId = ko.observable("TextColorInputId");
	self.TextColorValue = ko.observable(TextOBJ.defaultStyle.strokeColor);
	self.InitTextColor = function(){
		
		//alert(TextOBJ.defaultStyle.strokeColor);
		var input = document.getElementById(self.TextColorInputId());
		if(input !== null){
			
			input.addEventListener('change', function(e){
				var color = '#' + this.color;
				
				if(TextOBJ.activeTextId !== undefined){
					var activeText = JSXMaster.board.objects[TextOBJ.activeTextId];
					activeText.setAttribute({strokeColor: color});
				}else{
					TextOBJ.defaultStyle.strokeColor = color;
				}
			});
			
		}
	}
	//------------------------------------------------------------------------//
	self.PolygonBarId = ko.observable("polygon_toolbar_id");
	self.RectBarId = ko.observable("square_toolbar_id");
	self.CircleBarId = ko.observable("circle_toolbar_id");
	self.ShowOnOffBar = function(type){
		var status = self.ToolbarList[type];
		if(status === undefined) return;
		(status() === 'none') ? self.ActivateItem(type) : self.ActivateItem('');
	}
	//------------------------------------------------------------------------//

	self.DragActive = ko.observable("");
	self.RectActive = ko.observable("");
	self.CircleActive = ko.observable("");
	self.LineActive = ko.observable("");
	self.PolygonActive = ko.observable("");
	self.ZoomActive = ko.observable("");
	self.TextActive = ko.observable("");
	self.PolygonBarStatus = ko.observable('none');
	self.CircleBarStatus = ko.observable('none');
	self.RectBarStatus = ko.observable('none');
	
	self.ItemList = {};
	self.ToolbarList = {};
	self.ItemList["drag"] = self.DragActive;
	self.ItemList["rectangle"] = self.RectActive;
	self.ItemList["circle"] = self.CircleActive;
	self.ItemList["line"] = self.LineActive;
	self.ItemList["polygon"] = self.PolygonActive;
	self.ItemList['zoom'] = self.ZoomActive;
	self.ItemList['text'] = self.TextActive;
	
	self.ToolbarList['zoom'] = self.ZoomBarStatus;
	self.ToolbarList['line'] = self.LineBarStatus;
	self.ToolbarList['text'] = self.TextBarStatus;
	self.ToolbarList['rectangle'] = self.RectBarStatus;
	self.ToolbarList['polygon'] = self.PolygonBarStatus;
	self.ToolbarList['circle'] = self.CircleBarStatus;

	self.ActivateItem = function(item){
		for(var key in self.ItemList){
			self.ItemList[key]('');
		}
		for(var key in self.ToolbarList){
			self.ToolbarList[key]('none');
		}
		var it = self.ItemList[item];
		if(it !== undefined){	
			it('toolbar_item_click');
		}
		var toolbar = self.ToolbarList[item];
		if(toolbar !== undefined){
			toolbar('block');
			
		}
	}
	//----------------Line bar
	
};
GUI.prototype = {
	deleteObj : function(obj){
		JSXMaster.board.removeObject(obj);
	},
	deleteDict: function(dict){
		//TODO
	}
}
var gui = new GUI();
gui.init();
ko.applyBindings(gui);

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
       


