var app = angular.module("myApp", [])
app.controller("MyController", function($scope, $http) {

	$scope.getDataFromServer = function() {
		$http({
			method : 'POST',
			url : $scope.resURL,
			dataType : "json",
		}).success(function(data, status, headers, config) {
			$scope.curPage = 0;
			$scope.pageSize = 3;
			$scope.lists = chunk(data, 3);
			$scope.numberOfPages = function() {
				return Math.ceil($scope.lists.length / $scope.pageSize);
			};

		}).error(function(data, status, headers, config) {
		});

	};
	
	function chunk(arr, size) {
		var newArr = [];
		for (var i = 0; i < arr.length; i += size) {
			newArr.push(arr.slice(i, i + size));
		}
		return newArr;
	}
});

app.filter('pagination', function() {
	return function(input, start) {
		if(input != undefined && start != NaN){
			start = +start;
			return input.slice(start);
		}
		
	};
});
