<%@ page import="com.liferay.portal.model.User" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="theme" %>
<portlet:defineObjects />
<theme:defineObjects />

<portlet:resourceURL var="resourceURL"></portlet:resourceURL>

<%-- <liferay-portlet:renderURL var="linkURL" portletName="image-process_WAR_Biziwaveportlet" windowState="maximized" portletMode="view">
	<portlet:param name="photoUid" value="" />
</liferay-portlet:renderURL> --%>

<%-- <% User userLiferay = themeDisplay.getUser();  %> --%>

<div ng-app="myApp" ng-init="resURL = '${resourceURL}'">

<div ng-controller="MyController" ng-if="resURL != undefined" ng-init="getDataFromServer()">
		<div ng-if="lists == undefined" class="loadingclass"><img src="<%=request.getContextPath()%>/img/loading1.gif"/></div>
		 <!-- Hot fix for demo -->
		<!-- <button type="button" class="btn btn-primary" ng-click="getDataFromServer()">Get data</button> -->
		<div  ng-repeat="rows in lists | pagination: curPage * pageSize | limitTo: pageSize">
			<div class="span4" ng-repeat="item in rows ">
			<%-- 	<span>Username: <%= userLiferay.getFullName() %></span><br> --%>
				<span>Username: {{::item.img_userName}}</span><br>
				<span>Image ID: <a href="./image-editor?pId={{item.img_id}}">{{::item.img_id}}</a></span> 
				<span ng-if="item != undefined"><br> 
				<!-- <img ng-src="data:image/gif;base64,{{::item.img_photo}}" class="imageclass"/> </span> -->
				 <img ng-src="{{::item.img_photo}}" class="imageclass"/> </span>
			</div>
			<br>
		</div>
		<!-- <div class="pagination pagination-centered" ng-show="lists.length"> -->
		<div class="pagination" ng-show="lists.length">
			<ul class="navigationIcon">
				<li>
					<button type="button" class="btn btn-primary" ng-disabled="curPage == 0" ng-click="curPage=curPage-1">&lt; PREV</button>
				</li>
				<!-- <li><span>Page {{curPage + 1}} of {{ numberOfPages() }}</span>
				</li> -->
				<li>
					<button type="button" class="btn btn-primary" ng-disabled="curPage >= lists.length/pageSize - 1" ng-click="curPage = curPage+1">NEXT &gt;</button>
				</li>
			</ul>
		</div>
	</div>
</div>

