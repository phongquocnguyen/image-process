<%-- <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<portlet:defineObjects />
<portlet:actionURL name="uploadPicture" var="uploadFileURL"></portlet:actionURL>
<aui:form action="<%= uploadFileURL %>" enctype="multipart/form-data" method="post">
 
	<aui:input type="file" name="fileupload" />
	
	<aui:button name="Save" value="Save" type="submit" />
 
</aui:form> --%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="com.liferay.portal.kernel.util.Validator"%>
<%@ page import="javax.portlet.PortletPreferences"%>
<%@ page import="com.liferay.util.PwdGenerator"%>
<portlet:defineObjects />
<%
String uploadProgressId = PwdGenerator.getPassword(PwdGenerator.KEY3, 4);
PortletPreferences prefs = renderRequest.getPreferences();
%>


<portlet:actionURL var="editCaseURL" name="uploadPicture">
   <portlet:param name="jspPage" value="/edit.jsp" />
</portlet:actionURL>
<liferay-ui:upload-progress id="<%= uploadProgressId %>" message="uploading" redirect="<%= editCaseURL %>"/>

<aui:form action="<%= editCaseURL %>" enctype="multipart/form-data" method="post" >
<aui:input type="file" name="fileupload"/>
<input type="submit" value="<liferay-ui:message key="upload" />" onClick="<%= uploadProgressId %>.startProgress(); return true;"/>
<!--  aui:button type="submit" value="Save" /-->
</aui:form>

































<!-- <form name="myForm">
	<fieldset>
		<legend>Upload on form submit</legend>
		Profile Picture: <input type="file" ngf-select="" ng-model="picFile"
			name="file" accept="image/*" required="">
		<button ng-disabled="!myForm.$valid" ng-click="uploadPic(picFile)">Submit</button>
		<img ng-show="picFile[0] != null" ngf-src="picFile[0]" class="thumb">
		<span class="progress" ng-show="picFile[0].progress >= 0">
			<div style="width: {{picFile[0].progress"
				ng-bind="picFile[0].progress + '%'" class="ng-binding"></div>
		</span> <span ng-show="picFile[0].result">Upload Successful</span>
	</fieldset>
	<br>
</form> -->

<%-- <div ng-app="fileUpload" ng-controller="MyCtrl" ng-init="resURL = '${uploadFileURL}'">
<form name="myForm" action="<%= uploadFileURL %>" enctype="multipart/form-data" method="post">
    <input type="file" name="fileupload" ngf-select="" ng-model="files" accept="image/*" required="">
    <button ng-disabled="!myForm.$valid" type="submit" ng-click="upload(files)">Submit</button>
    <img ng-show="files[0] != null" ngf-src="files[0]" class="thumb">
    <span class="progress" ng-show="files[0].progress >= 0">
			<div style="width: {{files[0].progress"
				ng-bind="files[0].progress + '%'" class="ng-binding"></div>
		</span>
</form>
    <pre>{{log}}</pre>
</div>

<style>
.button {
    -moz-appearance: button;
    /* Firefox */
    -webkit-appearance: button;
    /* Safari and Chrome */
    padding: 10px;
    margin: 10px;
    width: 70px;
}
.drop-box {
    background: #F8F8F8;
    border: 5px dashed #DDD;
    width: 200px;
    height: 65px;
    text-align: center;
    padding-top: 25px;
    margin: 10px;
}
.dragover {
    border: 5px dashed blue;
}
.thumb{
	width: 100px;
	height:100px;
}
.progress {
	display: inline-block;
	width: 100px;
	border: 3px groove #CCC;
}

.progress div {
	font-size: smaller;
	background: orange;
	width: 0;
}

</style>
 --%>