package com.biziwave;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.biziwave.dao.ImageDao;
import com.biziwave.util.DatabaseConnection;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.ProgressTracker;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ImageUpload
 */
public class ImageUpload extends MVCPortlet {
	private static Log LOGGER = LogFactoryUtil.getLog(ImageUpload.class);
	private final static String DOCUMENT_FOLDER = "ImageStorage";
	private final static String FILE_NAME = "fileupload";
	private Connection conn;

	public ImageUpload() {
		conn = DatabaseConnection.getConnection();
		LOGGER.info("Connection " + conn);
	}

	public void uploadPicture(ActionRequest request, ActionResponse response) throws Exception {

		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		File filePart = uploadRequest.getFile("photo");
		LOGGER.info("Image File: " + filePart);
		
		
		final String uploadProgressId = ParamUtil.getString(request, "uploadProgressId");
		 
		 final ProgressTracker progressTracker = new ProgressTracker(uploadProgressId);
		 progressTracker.start(request);
		 
		 int percentage = 10;
		 final int total = 1000000;
		 
		 for (int i = 0; i < total; i++) {
		        percentage = Math.min(10 + (i * 90) / total, 99);
		        progressTracker.setPercent(percentage);
		 }
		 progressTracker.finish(request);
		
	
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long userid = themeDisplay.getUserId();
		LOGGER.info("UserID: " + userid);
		String userFullName = themeDisplay.getUser().getFullName(); 
		LOGGER.info("Username:" + userFullName);
	
		LOGGER.info("****************Upload File*******************");
		Folder tempUploadedFileFolder = null;
		FileEntry fileEntry = null;
		
		//UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		String tempFileName = uploadRequest.getFileName(FILE_NAME);
		LOGGER.info("Upload Filename: " + tempFileName);
		File tempFile = uploadRequest.getFile(FILE_NAME);
		LOGGER.info("Temp FilePath: " + tempFile);
		
		if (tempFile != null && tempFileName != null) {
			try {
				long repositoryId = DLFolderConstants.getDataRepositoryId(themeDisplay.getScopeGroupId(),DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
				LOGGER.info("Repository: " + repositoryId);
				tempUploadedFileFolder = DLAppLocalServiceUtil.getFolder(repositoryId,DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,DOCUMENT_FOLDER);
				LOGGER.info("FileFolder: " + tempUploadedFileFolder);
				fileEntry = DLAppLocalServiceUtil.addFileEntry(
						themeDisplay.getUserId(),
						repositoryId,
						tempUploadedFileFolder.getFolderId(),
						tempFileName,
						uploadRequest.getContentType(),
						tempFile.getName(),
						tempFile.getName(),
						"changeLog ",
						tempFile,
						ServiceContextFactory.getInstance(DLFileEntry.class.getName(), request));
				LOGGER.info("FileEntry: " + fileEntry);
				String portalURL = PortalUtil.getPortalURL(themeDisplay);
				String photoUrl = portalURL + "/c/document_library/get_file?uuid=" + fileEntry.getUuid() + "&groupId=" + fileEntry.getGroupId();
				LOGGER.info("Image URL:" + photoUrl);			
				
				ImageDao dao = new ImageDao();
				dao.InsertImages(conn, String.valueOf(userid), userFullName, photoUrl);
			
			} catch (IOException ex) {
				LOGGER.error("Error during converting image.", ex);
			} catch (SQLException ex) {
				LOGGER.error("Fail to save to DB" + ex);
			} catch (Exception ex) {
				LOGGER.error("System exception in file Upload", ex);
			}
		}
		

	}
	
			

}
