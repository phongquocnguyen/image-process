package com.biziwave.dto;

/**
 * The Class ImageInfo.
 */
public class ImageInfo {

  /** The width. */
  private int width;

  /** The height. */
  private int height;

  /** The pixels. */
  private long pixels;

  /** The ruler x. */
  private int rulerX;

  /** The ruler y. */
  private int rulerY;

  /** The scale x. */
  private double scaleX;

  /** The scale y. */
  private double scaleY;

  /**
   * Gets the width.
   *
   * @return the width
   */
  public int getWidth() {
    return width;
  }

  /**
   * Sets the width.
   *
   * @param width
   *          the width
   */
  public void setWidth(int width) {
    this.width = width;
  }

  /**
   * Gets the height.
   *
   * @return the height
   */
  public int getHeight() {
    return height;
  }

  /**
   * Sets the height.
   *
   * @param height
   *          the height
   */
  public void setHeight(int height) {
    this.height = height;
  }

  /**
   * Gets the pixels.
   *
   * @return the pixels
   */
  public long getPixels() {
    return pixels;
  }

  /**
   * Sets the pixels.
   *
   * @param pixels
   *          the pixels
   */
  public void setPixels(long pixels) {
    this.pixels = pixels;
  }

  /**
   * Gets the ruler x.
   *
   * @return the ruler x
   */
  public int getRulerX() {
    return rulerX;
  }

  /**
   * Sets the ruler x.
   *
   * @param rulerX
   *          the ruler x
   */
  public void setRulerX(int rulerX) {
    this.rulerX = rulerX;
  }

  /**
   * Gets the ruler y.
   *
   * @return the ruler y
   */
  public int getRulerY() {
    return rulerY;
  }

  /**
   * Sets the ruler y.
   *
   * @param rulerY
   *          the ruler y
   */
  public void setRulerY(int rulerY) {
    this.rulerY = rulerY;
  }

  /**
   * Gets the scale x.
   *
   * @return the scale x
   */
  public double getScaleX() {
    return scaleX;
  }

  /**
   * Sets the scale x.
   *
   * @param scaleX
   *          the scale x
   */
  public void setScaleX(double scaleX) {
    this.scaleX = scaleX;
  }

  /**
   * Gets the scale y.
   *
   * @return the scale y
   */
  public double getScaleY() {
    return scaleY;
  }

  /**
   * Sets the scale y.
   *
   * @param scaleY
   *          the scale y
   */
  public void setScaleY(double scaleY) {
    this.scaleY = scaleY;
  }

  /**
   * {@inheritDoc}
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "ImageInfo [width=" + width + ", height=" + height + ", pixels=" + pixels
        + ", rulerX=" + rulerX + ", rulerY=" + rulerY + "]";
  }
}
