package com.biziwave.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.biziwave.ImageUpload;
import com.biziwave.dto.ImageObject;
import com.biziwave.util.DatabaseConnection;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class ImageDao {
	private static Log LOGGER = LogFactoryUtil.getLog(ImageUpload.class);
	private Connection conn;

	public ImageDao() {
		conn = DatabaseConnection.getConnection();
		LOGGER.info("Connection " + conn);
	}
	
	public ArrayList<ImageObject> GetImages(Connection connection) throws Exception {
		ArrayList<ImageObject> messageData = new ArrayList<ImageObject>();
		String sql = "SELECT image_id,photo_url,user_name FROM image ORDER BY image_id DESC";
		try {
			PreparedStatement ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				ImageObject imageObject = new ImageObject();
				imageObject.setImg_id(rs.getString("image_id"));
				imageObject.setImg_photo(rs.getString("photo_url"));
				imageObject.setImg_userName((rs.getString("user_name")));				
				messageData.add(imageObject);
				
				/****** Insert Blob Image ***********************************
				byte[] imageBytes;
				String imageBase64;
				imageBytes = rs.getBytes("photo");
				imageBase64 = DatatypeConverter.printBase64Binary(imageBytes);
				messageObject.setImg_photo(imageBase64);
				*************************************************************/

			}
			/*return messageData;*/
		} catch (Exception e) {
			LOGGER.error("System exception in Get Images", e);
		}
		return messageData;
	}
	
	public void InsertImages(Connection connection, String userId, String userName, String photoUrl) throws Exception {
		try {
			String sql = "INSERT INTO image (user_id,user_name,photo_url) values (?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setString(1,userId);
			ps.setString(2,userName);
			ps.setString(3,photoUrl); 

			int row = ps.executeUpdate();
			if (row > 0) {
				LOGGER.info("Save Image to DB successful");
			}
		} catch (SQLException ex) {
			LOGGER.error("Fail to save to DB" + ex);
		} catch (Exception ex) {
			LOGGER.error("System exception in Upload Image", ex);
		}
	}
	
	
	
	

}