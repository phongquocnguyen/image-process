package com.biziwave.iface;



import java.io.File;
import java.io.IOException;

import com.biziwave.dto.ImageInfo;

/**
 * The Interface ImageScanner.
 */
public interface ImageScanner {

  /**
   * Scan image.
   *
   * @param file
   *          the file
   * @return the image info
   * @throws IOException
   *           the IO exception
   */
  ImageInfo scan(File file) throws IOException;
}
