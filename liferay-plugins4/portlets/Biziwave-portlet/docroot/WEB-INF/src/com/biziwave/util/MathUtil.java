package com.biziwave.util;


import java.util.List;

/**
 * The Class MathUtil.
 */
public class MathUtil {

  /**
   * Average.
   *
   * @param list
   *          the list
   * @return the double
   */
  public static double average(List<Integer> list) {
    long sum = 0;
    int count = 0;
    for (Integer integer : list) {
      if (integer != null) {
        sum += integer.intValue();
        count++;
      }
    }
    return sum / (double) count;

  }
}
