package com.biziwave;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.biziwave.dao.ImageDao;
import com.biziwave.dto.ImageObject;
import com.biziwave.iface.ImageScanner;
import com.biziwave.util.DatabaseConnection;
import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ImageProcess
 */
public class ImageProcess extends MVCPortlet {
	private static Log LOGGER = LogFactoryUtil.getLog(ImageProcess.class);
	private Connection conn;
	/**For Image Process **/ 
	private final static String DOCUMENT_FOLDER = "Hello";
	private final static String FILE_NAME = "fileupload";

	public ImageProcess() {
		// super();
		conn = DatabaseConnection.getConnection();
		LOGGER.info("Connection " + conn);
	}
	public void serveResource(ResourceRequest resourceRequest,ResourceResponse resourceResponse) throws IOException,PortletException {
		try {
			LOGGER.info("Ajax Loading...");
			ArrayList<ImageObject> imageData = null;
			ImageDao dao = new ImageDao();
			imageData = dao.GetImages(conn);
			
			PrintWriter writer = resourceResponse.getWriter();
			//JSONObject objJsonObject = JSONFactoryUtil.createJSONObject();
			Gson gson = new Gson();
			String messages = gson.toJson(imageData);
			LOGGER.info(messages);
			
			writer.print(messages);
		} catch (Exception e) {
			LOGGER.error(e);
	
		}
	}
	
	public void uploadImage(ActionRequest request, ActionResponse response) throws Exception {

		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		File filePart = uploadRequest.getFile("photo");
		LOGGER.info("Image File: " + filePart);
	
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long userid = themeDisplay.getUserId();
		LOGGER.info("UserID: " + userid);
		String userFullName = themeDisplay.getUser().getFullName(); 
		LOGGER.info("Username:" + userFullName);
	
		LOGGER.info("****************Upload File*******************");
		Folder tempUploadedFileFolder = null;
		FileEntry fileEntry = null;
		
		String tempFileName = uploadRequest.getFileName(FILE_NAME);
		LOGGER.info("Upload Filename: " + tempFileName);
		File tempFile = uploadRequest.getFile(FILE_NAME);
		LOGGER.info("Temp FilePath: " + tempFile);
		
		if (tempFile != null && tempFileName != null) {
			try {
				
				/*for Image Process Code */
			    ImageScanner scanner = new ImageScannerImpl();
			    scanner.scan(tempFile);
			    
				long repositoryId = DLFolderConstants.getDataRepositoryId(themeDisplay.getScopeGroupId(),DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
				LOGGER.info("Repository: " + repositoryId);
				tempUploadedFileFolder = DLAppLocalServiceUtil.getFolder(repositoryId,DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,DOCUMENT_FOLDER);
				LOGGER.info("FileFolder: " + tempUploadedFileFolder);
				fileEntry = DLAppLocalServiceUtil.addFileEntry(
						themeDisplay.getUserId(),
						repositoryId,
						tempUploadedFileFolder.getFolderId(),
						tempFileName,
						uploadRequest.getContentType(),
						tempFile.getName(),
						tempFile.getName(),
						"changeLog ",
						tempFile,
						ServiceContextFactory.getInstance(DLFileEntry.class.getName(), request));
				LOGGER.info("FileEntry: " + fileEntry);
				String portalURL = PortalUtil.getPortalURL(themeDisplay);
				String photoUrl = portalURL + "/c/document_library/get_file?uuid=" + fileEntry.getUuid() + "&groupId=" + fileEntry.getGroupId();
				LOGGER.info("Image URL:" + photoUrl);	
				
				
				    
				ImageDao dao = new ImageDao();
				dao.InsertImages(conn, String.valueOf(userid), userFullName, photoUrl);
				
		    } catch (IOException e) {
		    	  LOGGER.error(e);
			} catch (SQLException ex) {
				LOGGER.error("Fail to save to DB" + ex);
			} catch (Exception ex) {
				LOGGER.error("System exception in file Upload", ex);
			}
		}
		

	}
	
}
