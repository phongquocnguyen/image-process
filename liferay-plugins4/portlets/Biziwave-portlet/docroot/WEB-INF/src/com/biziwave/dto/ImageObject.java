package com.biziwave.dto;

public class ImageObject {
	private String img_id;
	private String img_photo;
	private String img_userId;
	private String img_userName;

	public String getImg_userName() {
		return img_userName;
	}

	public void setImg_userName(String img_userName) {
		this.img_userName = img_userName;
	}

	public String getImg_userId() {
		return img_userId;
	}

	public void setImg_userId(String img_userId) {
		this.img_userId = img_userId;
	}

	public String getImg_id() {
		return img_id;
	}

	public void setImg_id(String img_id) {
		this.img_id = img_id;
	}

	public String getImg_photo() {
		return img_photo;
	}

	public void setImg_photo(String img_photo) {
		this.img_photo = img_photo;
	}

}
