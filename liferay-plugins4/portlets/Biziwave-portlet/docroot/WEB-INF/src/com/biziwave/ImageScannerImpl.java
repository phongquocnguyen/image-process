package com.biziwave;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.biziwave.dto.ImageInfo;
import com.biziwave.iface.ImageScanner;
import com.biziwave.util.MathUtil;



/**
 * The Class ImageScannerImpl.
 */
public class ImageScannerImpl implements ImageScanner {

  /** The Constant TEST_RULER_WHITE_NUM. */
  private static final int TEST_RULER_WHITE_NUM = 15;

  /** The Constant WHITE_RGB. */
  private static final int WHITE_RGB = Color.WHITE.getRGB();

  /** The Constant RGB_WHITE_INT. */
  private static final int RGB_WHITE_INT = 245;

  /**
   * {@inheritDoc}
   * 
   * @see com.ips.imageprocess.ImageScanner#scan(java.io.File)
   */
  @Override
  public ImageInfo scan(File file) throws IOException {
    if (file == null) {
      return null;
    }
    ImageInfo imageInfo = new ImageInfo();
    BufferedImage image = ImageIO.read(file);
    imageInfo.setWidth(image.getWidth());
    imageInfo.setHeight(image.getHeight());
    imageInfo.setPixels(imageInfo.getWidth() * imageInfo.getHeight());

    int halfWidth = imageInfo.getWidth() / 2;
    int halfHeight = imageInfo.getHeight() / 2;

    int[][] rgb = new int[imageInfo.getWidth()][imageInfo.getHeight()];
    for (int i = 0; i < imageInfo.getWidth(); i++) {
      for (int j = 0; j < imageInfo.getHeight(); j++) {
        rgb[i][j] = image.getRGB(i, j);
      }
    }

    // find Y ruler
    boolean found = false;
    int ix = imageInfo.getWidth() - 1;
    int jx = halfHeight;
    while (!found && ix >= halfWidth) {
      if (isWhite(rgb[ix][jx])) {
        if (isVerticalWhite(rgb[ix], jx, TEST_RULER_WHITE_NUM)) {
          found = true;
          List<Integer> samples = new ArrayList<Integer>();
          imageInfo.setRulerY(detectRulerY(rgb, ix, jx, samples));
          imageInfo.setScaleY(MathUtil.average(samples));
        }
      }
      ix--;
    }

    // find X ruler
    found = false;
    int iy = halfWidth;
    int jy = imageInfo.getHeight() - 1;
    while (!found && jy >= halfHeight) {
      if (isWhite(rgb[iy][jy])) {
        if (isHorizontalWhite(rgb, jy, iy, TEST_RULER_WHITE_NUM)) {
          found = true;
          List<Integer> samples = new ArrayList<Integer>();
          imageInfo.setRulerX(detectRulerX(rgb, iy, jy, samples));
          imageInfo.setScaleX(MathUtil.average(samples));
        }
      }
      jy--;
    }
    return imageInfo;
  }

  /**
   * Detect ruler y.
   *
   * @param rgb
   *          the rgb
   * @param x
   *          the x
   * @param y
   *          the y
   * @param samples
   *          the samples
   * @return the int
   */
  private int detectRulerY(int[][] rgb, int x, int y, List<Integer> samples) {
    int[] temp = rgb[x];
    boolean foundStart = false;
    boolean foundEnd = false;
    // find ruler start/end
    int start = y;
    while (!foundStart) {
      if (isWhite(temp[start - 1])) {
        start--;
      } else {
        foundStart = true;
      }
    }
    int end = y;
    while (!foundEnd) {
      if (isWhite(temp[end + 1])) {
        end++;
      } else {
        foundEnd = true;
      }
    }

    // find angle
    int peak = x;
    while (isWhite(rgb[peak - 1][start])) {
      peak--;
    }

    int midX = (x + peak) / 2;
    temp = rgb[midX];
    int count = 0;
    boolean lastWhite = false;
    int whiteIndex = start;

    for (int i = start; i <= end; i++) {
      if (isWhite(temp[i])) {
        if (!lastWhite) {
          count++;
          if (count > 1) {
            samples.add(Integer.valueOf(i - whiteIndex + 1));
          }
        }
        whiteIndex = i;
        lastWhite = true;
      } else {
        lastWhite = false;
      }
    }

    return count;
  }

  /**
   * Detect ruler x.
   *
   * @param rgb
   *          the rgb
   * @param x
   *          the x
   * @param y
   *          the y
   * @param samples
   *          the samples
   * @return the int
   */
  private int detectRulerX(int[][] rgb, int x, int y, List<Integer> samples) {
    boolean foundStart = false;
    boolean foundEnd = false;
    // find ruler start/end
    int start = x;
    while (!foundStart) {
      if (isWhite(rgb[start - 1][y])) {
        start--;
      } else {
        foundStart = true;
      }
    }
    int end = x;
    while (!foundEnd) {
      if (isWhite(rgb[end + 1][y])) {
        end++;
      } else {
        foundEnd = true;
      }
    }

    // find angle
    int peak = y;
    while (isWhite(rgb[start][peak - 1])) {
      peak--;
    }

    int midY = (y + peak) / 2;
    int count = 0;
    boolean lastWhite = false;
    int whiteIndex = start;

    for (int i = start; i <= end; i++) {
      if (isWhite(rgb[i][midY])) {
        if (!lastWhite) {
          count++;
          if (count > 1) {
            samples.add(Integer.valueOf(i - whiteIndex + 1));
          }
        }
        whiteIndex = i;
        lastWhite = true;
      } else {
        lastWhite = false;
      }
    }

    return count;
  }

  /**
   * Checks if is white.
   *
   * @param rgb
   *          the rgb
   * @return true, if checks if is white
   */
  private boolean isWhite(int rgb) {
    boolean retVal = false;
    if (rgb == WHITE_RGB) {
      retVal = true;
    } else {
      Color c = new Color(rgb);
      if (c.getRed() >= RGB_WHITE_INT && c.getGreen() >= RGB_WHITE_INT && c.getBlue() >= RGB_WHITE_INT) {
        retVal = true;
      }
    }
    return retVal;
  }

  /**
   * Checks if is vertical white.
   *
   * @param rgb
   *          the rgb
   * @param index
   *          the index
   * @param count
   *          the count
   * @return true, if checks if is vertical white
   */
  private boolean isVerticalWhite(int[] rgb, int index, int count) {
    if (index >= count && rgb.length > index + count) {
      for (int i = index - count; i < index + count; i++) {
        if (!isWhite(rgb[i])) {
          return false;
        }
      }
      return true;
    } else {
      return false;
    }
  }

  /**
   * Checks if is horizontal white.
   *
   * @param rgb
   *          the rgb
   * @param col
   *          the col
   * @param index
   *          the index
   * @param count
   *          the count
   * @return true, if checks if is horizontal white
   */
  private boolean isHorizontalWhite(int[][] rgb, int col, int index, int count) {
    if (index >= count && rgb.length > index + count) {
      for (int i = index - count; i < index + count; i++) {
        if (!isWhite(rgb[i][col])) {
          return false;
        }
      }
      return true;
    } else {
      return false;
    }
  }

}
