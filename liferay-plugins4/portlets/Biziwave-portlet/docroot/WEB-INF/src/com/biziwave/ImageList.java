package com.biziwave;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.biziwave.dao.ImageDao;
import com.biziwave.dto.ImageObject;
import com.biziwave.util.DatabaseConnection;
import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ImageList
 */


public class ImageList extends MVCPortlet {
	private static Log LOGGER = LogFactoryUtil.getLog(ImageList.class);
	private Connection conn;

	public ImageList() {
		// super();
		conn = DatabaseConnection.getConnection();
		LOGGER.info("Connection " + conn);
	}

	public void serveResource(ResourceRequest resourceRequest,ResourceResponse resourceResponse) throws IOException,PortletException {
		try {
			LOGGER.info("Ajax Loading...");
			ArrayList<ImageObject> imageData = null;
			ImageDao dao = new ImageDao();
			imageData = dao.GetImages(conn);
			
			PrintWriter writer = resourceResponse.getWriter();
			//JSONObject objJsonObject = JSONFactoryUtil.createJSONObject();
			Gson gson = new Gson();
			String messages = gson.toJson(imageData);
			LOGGER.info(messages);
			
			writer.print(messages);
		} catch (Exception e) {
			LOGGER.error(e);

		}
	}
}