package com.biziwave;

import com.liferay.util.bridges.mvc.MVCPortlet;
import java.io.File;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
/**
 * Portlet implementation class UploadFile
 */
public class UploadFile extends MVCPortlet {
	private static Log LOGGER = LogFactoryUtil.getLog(UploadFile.class);
	private final static String DOCUMENT_FOLDER = "Hello";
	private final static String FILE_NAME = "fileupload";

	public void uploadPicture(ActionRequest request, ActionResponse response) throws Exception {
		LOGGER.info("****************Upload File*******************");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		Folder tempUploadedFileFolder = null;
		FileEntry fileEntry = null;
		
		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);
		String tempFileName = uploadRequest.getFileName(FILE_NAME);
		LOGGER.info("Upload Filename: " + tempFileName);
		File tempFile = uploadRequest.getFile(FILE_NAME);
		LOGGER.info("Temp FilePath: " + tempFile);
		
		if (tempFile != null && tempFileName != null) {
			try {
				long repositoryId = DLFolderConstants.getDataRepositoryId(themeDisplay.getScopeGroupId(),DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
				LOGGER.info("Repository: " + repositoryId);
				tempUploadedFileFolder = DLAppLocalServiceUtil.getFolder(repositoryId,DLFolderConstants.DEFAULT_PARENT_FOLDER_ID,DOCUMENT_FOLDER);
				LOGGER.info("FileFolder: " + tempUploadedFileFolder);
				fileEntry = DLAppLocalServiceUtil.addFileEntry(
						themeDisplay.getUserId(),
						repositoryId,
						tempUploadedFileFolder.getFolderId(),
						tempFileName,
						uploadRequest.getContentType(),
						tempFile.getName(),
						tempFile.getName(),
						"changeLog ",
						tempFile,
						ServiceContextFactory.getInstance(
								DLFileEntry.class.getName(), request));
				LOGGER.info("FileEntry: " + fileEntry);
				/*if (tempImageFileEntry != null) {
					
					
				}*/
				String portalURL = PortalUtil.getPortalURL(themeDisplay);
				String url = portalURL + "/c/document_library/get_file?uuid=" + fileEntry.getUuid() + "&groupId=" + fileEntry.getGroupId();
				LOGGER.info(url);
			} catch (Exception ex) {
				LOGGER.error("System exception in file Upload", ex);
			}
		}
	}


}
