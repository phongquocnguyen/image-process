CREATE TABLE image (  
	image_id SERIAL PRIMARY KEY,
	user_id varchar(45) DEFAULT NULL,  
	user_name varchar(45) DEFAULT NULL, 
	scalex int DEFAULT NULL,  
	scaley int DEFAULT NULL,  
	canvas varchar(100) DEFAULT NULL,  
	photo_url text
);