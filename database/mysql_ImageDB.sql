create database ImageDB;  
  
use ImageDB;  
  
CREATE TABLE `image` (  
	`image_id` int NOT NULL AUTO_INCREMENT,  
	`user_id` varchar(45) DEFAULT NULL,  
	`user_name` varchar(45) DEFAULT NULL, 
	`scalex` int DEFAULT NULL,  
	`scaley` int DEFAULT NULL,  
	`canvas` varchar(100) DEFAULT NULL,  
	`photo_url` text,
  PRIMARY KEY (`image_id`)  
) ENGINE=InnoDB DEFAULT CHARSET=latin1  